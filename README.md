snmpe
=====

An OTP application Windows and Linux (need debug)

install rebar3 https://www.rebar3.org/ 

install erlang https://www.erlang.org/downloads/22.0

OTP 22.0 Windows 32-bit Binary File (91806805)

OTP 22.0 Windows 64-bit Binary File (94094976)

dowload rebar3 https://s3.amazonaws.com/rebar3/rebar3


Build
-----

    $ rebar3 compile 
     
    
Start
-----
 cd _build\default\lib\snmpe\ebin
 
 erl -config ./sys 
 
 1> application:start(snmpe).

![alt text](<conf.png>) 

![alt text](<ets.png>) 

![alt text](<get.png>) 

![alt text](<test.png>) 
