%% %CopyrightBegin%
%% 
%% Copyright Ericsson AB 2009-2012. All Rights Reserved.
%% 
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%% 
%% %CopyrightEnd%

%% @docfile "snmpe.hrl"
%% @author A. A. Gog <a.gog@list.ru>
%% @copyright 2020 A. A. Gog



%%%-------------------------------------------------------------------
%% @doc snmpe top level supervisor.
%% @end
%%%-------------------------------------------------------------------


-module(snmpe1_sup).

-behaviour(supervisor).
-include("./include/snmp_debug.hrl").

%% API
-export([start_link/0,stop/0,start_snmpe/2]).
-define (Mod, snmp_e_manager:start_link()).


%% Supervisor callbacks
-export([init/1]).

-define(SERVER, ?MODULE).

%%====================================================================
%% API functions
%%====================================================================

%start_link() ->
%    supervisor:start_link({local, ?SERVER}, ?MODULEULE, []).


start_link() ->
    ?d("start_link -> entry",[]),
    SupName = {local, ?MODULE},
    io:format("SupName: ~p~n ",[SupName]),
    supervisor:start_link(SupName, ?MODULE , []),
    snmp_e_server:start_link().



stop() ->
    ?d("stop -> entry", []),
    case whereis(?SERVER) of
	Pid when is_pid(Pid) ->
	    ?d("stop -> Pid: ~p", [Pid]),
	    exit(Pid, shutdown),
	    ?d("stop -> stopped", []),
	    ok;
	_ ->
	    ?d("stop -> not running", []),
	    not_running
    end.
    

start_snmpe(Type, Opts) ->
    io:format("start_sup_child----------"),
    ?d("start_snmpe -> entry with"
	"~n   Type: ~p"
	"~n   Opts: ~p", [Type, Opts]),
    Restart = get_restart(Opts, transient), 
    start_sup_child(snmpe_supervisor, Restart, [Type, Opts]).



%%====================================================================
%% Supervisor callbacks
%%====================================================================

%% Child :: {Id,StartFunc,Restart,Shutdown,Type,Modules}
%init([]) ->
%	snmpe1:start(),
%    {ok, { {one_for_all, 0, 1}, []} }.

%%--------------------------------------------------------------------
%% Func: init/1
%% Returns: {ok,  {SupFlags,  [ChildSpec]}} |
%%          ignore                          |
%%          {error, Reason}   
%%--------------------------------------------------------------------
init(_Args) ->
    ?d("init -> entry", []),

    {ok, {{one_for_one, 3, 100},
	  [{tag1, 
%	    {snmpe_server , start_link,[]},
 	    {snmp_e_manager, start_link , []},
%	    permanent
	    transient, 
	    10000, 
	    worker,[snmpe]
	    }
	  ]}}.



%%====================================================================
%% Internal functions
%%====================================================================
get_restart(Opts, Def) ->
    io:format("get_opt--restart--------"),
    get_opt(Opts, restart_type, Def).
get_opt(Opts, Key, Def) ->
    io:format("get_opt----------"),
    snmp_misc:get_option(Key, Opts, Def).

start_sup_child(Mod, Type, Args) ->
    Spec = sup_spec(Mod, Type, Args),
    io:format("start_sup_child----------"),   
    supervisor:start_child(?MODULE, Spec).

sup_spec(Name, Type, Args) ->
    {Name, 
     {Name, start_link, Args}, 
     Type, 2000, supervisor, [Name, supervisor]}.
