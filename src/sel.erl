%% %CopyrightBegin%
%% 
%% Copyright Ericsson AB 2009-2012. All Rights Reserved.
%% 
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%% 
%% %CopyrightEnd%

%% @docfile "snmpe.hrl"
%% @author A. A. Gog <a.gog@list.ru>
%% @copyright 2020 A. A. Gog


-module(sel).
-export([start/1,start/2, out/1]).
 
start(mib) ->    
    L = ets:select(snmpm_mib_table,[{{'$1','$2','$3'},[],['$2']}]),
    out(L).

start(oid,Mib_oi) ->
    Mib_oid = case Mib_oi==undefined of
	true ->   L = ets:select(snmpm_mib_table,[{{'$1','$2','$3'},[],['$2']}]),
		  lists:nth(1,L);
	false -> list_to_atom(Mib_oi)
    end,	      
    F=ets:select(snmpm_mib_table,[{{'$1','$2','$3', Mib_oid},[{'=/=','$3',undefined}],['$1']}]),
    out(F).

 out(L)-> out(L,[]).
 out([H|T], Out) -> 
    case H of
	{mini_mib,F} -> out(T,[(lists:append(F,[0]))|Out]);
        _ -> out(T,[atom_to_list(H)|Out])
    end;
 out([], Out)->{lists:sort(Out)}.




