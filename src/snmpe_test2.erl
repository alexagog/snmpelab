%% %CopyrightBegin%
%% 
%% Copyright Ericsson AB 2009-2012. All Rights Reserved.
%% 
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%% 
%% %CopyrightEnd%

%% @docfile "snmpe.hrl"
%% @author A. A. Gog <a.gog@list.ru>
%% @copyright 2020 A. A. Gog
 
-module(snmpe_test2).

-behaviour(wx_object).

-export([start/1, init/1, terminate/2,  code_change/3, out_vbs/3, out_vbs_io/2, out_vbs_t/5,
	 handle_info/2, handle_call/3, handle_cast/2, handle_event/2]).

-include_lib("wx/include/wx.hrl").
-include_lib("./include/snmp_debug.hrl").
-include_lib("./include/snmp_types.hrl").
-include_lib("./include/snmp_verbosity.hrl").

-define(Get,10).
-define(Get_next,11).
-define(Get_bulk,12).
-define(ID_NOTEBOOK, 13).

-define(stc, wxStyledTextCtrl).
-define(stt, wxTreeCtrl).
-define(DEF_TARGETNAME, "default_agent").

-define(CONF_M,"querys_m.conf").

-define(N0, 0).
-define(N1, 1).
-define(N2, 2).
-define(N3, 3).
-define(N4, 4).
-define(N5, 5).
-define(N6, 6).
-define(N7, 7).

-define(confdir,snmpm_config:system_info(config_dir)). 
-record(state, 
	{
	  parent,
	  config,
	  command,
	  mib
	 }).


start(Config) ->
    wx_object:start_link(?MODULE, Config, []).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
init(Config) ->
        wx:batch(fun() -> do_init(Config) end).

do_init(Config) ->
    Parent = proplists:get_value(parent, Config),  
    Panel = wxPanel:new(Parent, []),

    %% Setup sizers
    MainSizer = wxBoxSizer:new(?wxVERTICAL),
    Sizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
 				 [{label, "Command single line"}]),

    Sizer3 = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				  [{label, "Snmp results"}]),

    TextCtrl3 = ?stc:new(Panel,[{id,3}]),
    ?stc:styleClearAll(TextCtrl3),
    ?stc:setLexer(TextCtrl3, ?wxSTC_LEX_ERLANG),

    wxWindow:connect(Panel, command_button_clicked),

    %% Add to sizers

    wxSizer:add(Sizer3, TextCtrl3, [{flag, ?wxEXPAND}, {proportion, 1}]),
    wxSizer:add(MainSizer, Sizer,  [{flag, ?wxEXPAND}]),
    wxSizer:addSpacer(MainSizer, 1),
    wxSizer:addSpacer(MainSizer, 1),
    wxSizer:add(MainSizer, Sizer3, [{flag, ?wxEXPAND}, {proportion, 1}]),

    wxPanel:setSizer(Panel, MainSizer),

    ets:new(snmpm_query_table, [ordered_set, protected, named_table, {keypos, 1}]),
	
%% -- Querys config --
    ?vdebug("querys config", []),
    Querys = read_query_config_file(?confdir),
%    io:format("Query: ~p~n",[Querys]), 
    init_querys_config(Querys),

    ListCtrl1 = create_list_ctrl(Panel, [{style, ?wxLC_REPORT}]),
    wxSizer:add(Sizer,ListCtrl1,[{flag, ?wxEXPAND}]),
    wxListCtrl:connect(ListCtrl1, command_list_item_selected, []),
    {Panel, #state{parent=Panel, config=Config}}.

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Async Events are handled in handle_event as in handle_info
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

handle_event(#wx{obj = _ListCtrl,
		 event = #wxList{itemIndex = Item}},
                 State = #state{}) ->
    [{_,R1,_R2,_R3,R4,_,_,_}]=ets:lookup(snmpm_query_table,Item),
    io:format("Main out=====~n ~p~n ~p~n ~p~n ~p~n ",[R1,_R2,_R3,Item]),  
    snmp_query_get(R1,R4,Item,State),

    {noreply,State}.


%---------------------------------------------------------------------------
%% Callbacks handled as normal gen_server callbacks
handle_info(Msg, State) ->
    snmpe1:format(State#state.config, "Got Info ~p\n",[Msg]),
    {noreply, State}.

handle_call(shutdown, _From, State=#state{parent=Panel}) ->
    wxPanel:destroy(Panel),
%    io:format("Shutdonw=~p~n,~p~n",[_From,[State]]),
    {stop, normal, ok, State};

handle_call(Msg, _From, State) ->
    snmpe1:format(State#state.config,"Got Call ~p\n",[Msg]),
    {reply, {error,nyi}, State}.

handle_cast(Msg, State) ->
    io:format("Got cast ~p~n",[Msg]),
    {noreply,State}.

code_change(_, _, State) ->
    {stop, ignore, State}.

terminate(_Reason, _State) ->
    ok.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Local functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%read_query_config_file()->  
%    Dir=ets:lookup(snmpm_config_table,config_dir),
%    read_query_config_file(Dir).


read_query_config_file(Dir) ->
    Order = fun snmp_conf:no_order/2,
    Check = fun check_query_config/2,
    try read_file(Dir, ?CONF_M,Order,Check)
    catch
	throw:Error ->
	    ?vlog("query config error: ~p", [Error]),
	    erlang:raise(throw, Error)
    end.

check_query_config(Query,State) ->
%     io:format("Got query:: ~p~n",[Query]),
    {ok, Query_out} = check_query_config(Query),
    {{ok, Query_out}, State}.

 
%% Check query
check_query_config(
  {TargetName,NR,MR,Oids,IP,Port,FQDN}) when is_atom(TargetName) ->
    check_query_config(TargetName,NR,MR,Oids,IP,Port,FQDN);
check_query_config(
  {TargetName,NR,MR,Oids,IP,Port,FQDN}) when is_integer(NR) ->
    check_query_config(TargetName,NR,MR,Oids,IP,Port,FQDN);
check_query_config(
  {TargetName,NR,MR,Oids,IP,Port,FQDN}) when is_integer(MR) ->
    check_query_config(TargetName,NR,MR,Oids,IP,Port,FQDN);
check_query_config(
  {TargetName,NR,MR,Oids,IP,Port,FQDN}) when is_list(Oids) ->
    check_query_config(TargetName,NR,MR,Oids,IP,Port,FQDN);
check_query_config(
  {TargetName,NR,MR,Oids,IP,Port,FQDN}) when is_tuple(IP) ->
    check_query_config(TargetName,NR,MR,Oids,IP,Port,FQDN);
check_query_config(
  {TargetName,NR,MR,Oids,IP,Port,FQDN}) when is_atom(Port) ->
    check_query_config(TargetName,NR,MR,Oids,IP,Port,FQDN);
check_query_config(
  {TargetName,NR,MR,Oids,IP,Port,FQDN}) when is_atom (FQDN) ->
    check_query_config(TargetName,NR,MR,Oids,IP,Port,FQDN);
check_query_config(Query) ->
    error({bad_query_config, Query}).

check_query_config(TargetName,NR,MR,Oids,IP,Port,FQDN) ->
    ?vdebug("check_agent_config -> entry with"
	    "~n   TargetName: ~p", [TargetName]),
    snmp_conf:check_string(TargetName, {gt, 0}),
    Qout ={TargetName,NR,MR,Oids,IP,Port,FQDN}, 
%    io:format("For conf snmp: ~p~n",[Qout]),
    {ok, {Qout}}.


read_file(Dir, FileName, Order, Check) ->
    {ok,Path}=Dir, 
%    io:format("For read snmp: ~p~n ~p~n  ~p~n",[Path, Order, Check]),
    try snmp_conf:read(filename:join(Path, FileName),Order,Check)
    catch
	{error, Reason} when element(1, Reason) =:= failed_open ->
        ?vlog("failed reading config from ~s: ~p", [FileName, Reason])
    end.

 
init_querys_config([]) ->
    ok;

init_querys_config([Query|Querys]) ->
%    io:format("Q: ~p~n Qs: ~p~n",[Query,Querys]),
    init_query_config(Query),
    init_querys_config(Querys).

init_query_config({Query}) ->
    case load_agents(Query) of
	ok ->
	    ok;
	Error ->
	    throw(Error)
    end.


 load_agents(Query) ->
    ?vtrace("do_update_agent_info -> entry with~n"
%	    "   TargetName: ~p~n"
	    "   Info:       ~p", [Query]),
    No=ets:info(snmpm_query_table,size),
    Query2=erlang:insert_element(1,Query,No),
% io:format("Query: ~p~n", [Query2]),
	      ets:insert(snmpm_query_table,Query2),
ok.
       
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Local functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

create_list_ctrl(Win, Options) ->
    ListCtrl = wxListCtrl:new(Win, Options),
    wxListCtrl:insertColumn(ListCtrl, ?N0, "Id", []),
    wxListCtrl:insertColumn(ListCtrl, ?N1, "Target", []),
    wxListCtrl:insertColumn(ListCtrl, ?N2, "NR", []),
    wxListCtrl:insertColumn(ListCtrl, ?N3, "MR", []),
    wxListCtrl:insertColumn(ListCtrl, ?N4, "Oid", []),
    wxListCtrl:insertColumn(ListCtrl, ?N5, "IP", []),
    wxListCtrl:insertColumn(ListCtrl, ?N6, "Port", []),
    wxListCtrl:insertColumn(ListCtrl, ?N7, "FQDN", []),

    wxListCtrl:setColumnWidth(ListCtrl, 0, 40),
    wxListCtrl:setColumnWidth(ListCtrl, 1, 180),
    wxListCtrl:setColumnWidth(ListCtrl, 2, 40),
    wxListCtrl:setColumnWidth(ListCtrl, 3, 40),
    wxListCtrl:setColumnWidth(ListCtrl, 4, 150),
    wxListCtrl:setColumnWidth(ListCtrl, 5, 100),
    wxListCtrl:setColumnWidth(ListCtrl, 6, 40),
    wxListCtrl:setColumnWidth(ListCtrl, 7, 200),

    Rows=ets:tab2list(snmpm_query_table),

    Fun =
	fun(Int) ->
		Count=wxListCtrl:getItemCount(ListCtrl),
		wxListCtrl:insertItem(ListCtrl,Count, ""),
           case Count rem 2 of
             0 -> wxListCtrl:setItemBackgroundColour(ListCtrl, Count, {240,240,240,255});
	     _ -> ok  
	   end,    
		wxListCtrl:setItem(ListCtrl, Count, ?N0, integer_to_list(element(1,Int))),
		wxListCtrl:setItem(ListCtrl, Count, ?N1, element(2,Int)),
		wxListCtrl:setItem(ListCtrl, Count, ?N2, integer_to_list(element(3,Int))),
		wxListCtrl:setItem(ListCtrl, Count, ?N3, integer_to_list(element(4,Int))),

		Odt=io_lib:format("~p",[element(5,Int)]), 
		Str_Oids=lists:flatten(Odt),
		wxListCtrl:setItem(ListCtrl, Count, ?N4, Str_Oids),

		OdtIP=io_lib:format("~p",[element(6,Int)]), 
		Str_IP=lists:flatten(OdtIP),
		wxListCtrl:setItem(ListCtrl, Count, ?N5, Str_IP),
 
		wxListCtrl:setItem(ListCtrl, Count, ?N6,integer_to_list(element(7,Int))),
		wxListCtrl:setItem(ListCtrl, Count, ?N7, element(8,Int))
	end,

   wx:foreach(Fun, Rows),
   ListCtrl.

%%%-----------------------------------------------------------------------------------
%% Out that all varbinds have the expected name and type
%%%-----------------------------------------------------------------------------------
 out_vbs_io(Vbs,ios)->
    (catch out_vbs_io(Vbs,ios,[])).
 out_vbs_io([],ios,Acc) -> 
    AccR=lists:reverse(Acc),
    {ok, AccR};
 out_vbs_io([Vb|T],ios,Acc) ->
    Val  = out_vb(Vb),
%    Ac = lists:flatten(io_lib:format("~p", [Val])),
 out_vbs_io(T,ios,[Val|Acc]).
%    io:format("val:~p~n",[Ac]),
%    Val.

%--out-win--------------------------------------------------
out_vbs({noError, 0, Vbs},test,WinOut) ->
    (catch out_vbs(Vbs,test,"",WinOut));
out_vbs (Res,_,_WinOut) ->
    {error, {unexpected_result, Res}}.

out_vbs([],test,Acc,_WOut) -> 
    {ok, Acc};
out_vbs([Vb|T],test,_Acc,WOut) ->
    Val  = out_vb(Vb),
%   io:format("~nVal: ~p ",[Val]),
    Acc2 = lists:flatten(io_lib:format("~p", [Val])),
%    Acc2 = lists:flatten (binary:list_to_bin(Val)),
%    ?stc:addText(WOut,io_lib:format("~200p~n",[Val])),
   ?stc:insertText(WOut,0,io_lib:format("~200p~n",[Val])),
    out_vbs(T,test,Acc2,WOut),
    Val.
out_vb(#varbind{oid = Oid, variabletype = Type, value = Val_i}) -> 
%      io:format("~p, ~p, ~p~n",[Oid,Type,Val_i]),
      Val = case Type=='OCTET STRING' of
	  true ->   case io_lib:printable_list(Val_i) of
		        false ->  lists:reverse(tl(lists:reverse(Val_i))); 
	 	        _-> Val_i
		    end;
	  _ -> Val_i
      end,

      {Oid,Type,Val}.

%-out-----------------------------------------
out_vbs_t({noError, 0, Vbs},test,WinOut,Root,_OidR) ->
    (catch out_vbs_t(Vbs,test,"",WinOut,Root,_OidR));
out_vbs_t (Res,_,_WinOut,_Root,_OidR) ->
    {error, {unexpected_result, Res}}.

out_vbs_t([],test,Acc,_WOut,_Root,_OidR) ->
    {ok, Acc};
out_vbs_t([Vb|T],test,_Acc,WOut,Root,OidR) ->
    Val  = out_vb_t(Vb),
    {OidT,_TypeT,_ValT}=Val,
    Val2 = lists:flatten(io_lib:format("~w = ~p :~p", [OidT,_TypeT,_ValT])),
    {OidT,_TypeT,_ValT}=Val,
    NextC=case lists:last(OidT)==1  of
	true-> ?stt:appendItem(WOut, Root, Val2);
	false -> Root
    end,
    NextT=?stt:appendItem(WOut, NextC, Val2),
    ?stt:expand(WOut, NextC),
    NextN =case lists:last(OidT)==0 of
	true ->  Root;
	false -> case lists:last(OidT)==1 of
		     true ->NextC;		  
		     false ->Root
		end		
    end,	     

    case lists:prefix(OidR,OidT)  of
	true ->  Root;
	false -> NextT
    end,
		     
%    io:format("~nRoot: ~p:~p~n",[Root,NextM]),
%    ?stt:addText(WOut,io_lib:format("~200p~n",[Val])),
    out_vbs_t(T,test,Val2,WOut,NextN,OidT),
    Val.


%================================================================
% Main request 
%================================================================
snmp_query_get(R1,R4,Item,State = #state{parent=Parent}) ->
    CtrlOut=wxWindow:findWindowById(3, [{parent, Parent}]),
    TextCtrl3 = wx:typeCast(CtrlOut, ?stc),
    snmpe1:format(State#state.config,"Item ~p selected.\n , ~p , ~p~n",[[Item],R1,[R4]]),

%Time in milli-seconds.  Default is 30000.
case snmp_e_manager:sync_get(R1,[R4]) of
     {ok,Tesing,_Timer}->{_,_,[{varbind,_,Type,_Fan3,_}]}=Tesing,
        Count=['INTEGER','Counter32'],
	case lists:member(Type,Count) of
	    true ->	?stc:clearAll(TextCtrl3),
			wx:new(),
			Frame = wxFrame:new(wx:null(), ?wxID_ANY, "Observer",[{size, {450, 300}}, {style, ?wxDEFAULT_FRAME_STYLE}]),
			Panel = wxPanel:new(Frame, []),
			Notebook = wxNotebook:new(Panel, ?ID_NOTEBOOK, [{style, ?wxBK_DEFAULT}]),
%% Perf Viewer Panel
			PerfPanel = snmp_e_perf:start_link(Notebook, self(),R1,R4), 
			wxNotebook:addPage(Notebook, PerfPanel, "Load Charts", []),
			wxFrame:raise(Frame),
			wxFrame:setFocus(Frame),
			SysPid = wx_object:get_pid(PerfPanel),
			SysPid ! {active, node()},
			MainSizer = wxBoxSizer:new(?wxVERTICAL),
			wxSizer:add(MainSizer, Notebook, [{proportion, 1}, {flag, ?wxEXPAND}]),
			wxPanel:setSizer(Panel, MainSizer),
			wxFrame:show(Frame);
	    false-> ok
	end,
      out_vbs(Tesing,test,TextCtrl3);        		 
    {error,_W} -> ?stc:setText(TextCtrl3,io_lib:format("Error: ~p~n",[_W]))
end, 
  ok.
    


%-out-tree-line
out_vb_t(#varbind{oid = Oid, variabletype = Type, value = Val_i}) -> 
%      io:format("~p, ~p, ~p~n",[Oid,Type,Val_i]),
      Val = case Type=='OCTET STRING' of
	  true ->   case io_lib:printable_list(Val_i) of
		        false ->  lists:reverse(tl(lists:reverse(Val_i))); 
	 	        _-> Val_i
		    end;
	  _ -> Val_i
      end,
      {Oid,Type,Val}.

