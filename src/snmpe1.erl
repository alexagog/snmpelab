%% %CopyrightBegin%
%% 
%% Copyright Ericsson AB 2009-2012. All Rights Reserved.
%% 
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%% 
%% %CopyrightEnd%

%% @docfile "snmpe.hrl"
%% @author A. A. Gog <a.gog@list.ru>
%% @copyright 2020 A. A. Gog

-module(snmpe1).
 
-include_lib("wx/include/wx.hrl").

-behaviour(wx_object).
-export([start/0, start/1, start_link/0, start_link/1, format/3,
	 init/1, terminate/2,  code_change/3,
	 handle_info/2, handle_call/3, handle_cast/2, handle_event/2]).

-define(wtc, wxStyledTextCtrl).


-record(state, {win, demo, example, selector, log, code}).

%% For wx-2.9 usage
-ifndef(wxSTC_ERLANG_COMMENT_FUNCTION).
-define(wxSTC_ERLANG_COMMENT_FUNCTION, 14).
-define(wxSTC_ERLANG_COMMENT_MODULE, 15).
-define(wxSTC_ERLANG_COMMENT_DOC, 16).
-define(wxSTC_ERLANG_COMMENT_DOC_MACRO, 17).
-define(wxSTC_ERLANG_ATOM_QUOTED, 18).
-define(wxSTC_ERLANG_MACRO_QUOTED, 19).
-define(wxSTC_ERLANG_RECORD_QUOTED, 20).
-define(wxSTC_ERLANG_NODE_NAME_QUOTED, 21).
-define(wxSTC_ERLANG_BIFS, 22).
-define(wxSTC_ERLANG_MODULES, 23).
-define(wxSTC_ERLANG_MODULES_ATT, 24).
-endif.


start() ->
    start([]).

start(Debug) ->
    wx_object:start(?MODULE, Debug, []).

start_link() ->
    start_link([]).

start_link(Debug) ->
    wx_object:start_link(?MODULE, Debug, []).

format(Config,Str,Args) ->
    Log = proplists:get_value(log, Config),

    Styles =  [{?wxSTC_ERLANG_DEFAULT,  {0,0,0}},
	       {?wxSTC_ERLANG_COMMENT,  {160,53,35}},
	       {?wxSTC_ERLANG_VARIABLE, {150,100,40}},
	       {?wxSTC_ERLANG_NUMBER,   {5,5,100}},
	       {?wxSTC_ERLANG_KEYWORD,  {130,40,172}},
	       {?wxSTC_ERLANG_STRING,   {170,45,132}},
	       {?wxSTC_ERLANG_OPERATOR, {30,0,0}},
	       {?wxSTC_ERLANG_ATOM,     {0,0,0}},
	       {?wxSTC_ERLANG_FUNCTION_NAME, {64,102,244}},
	       {?wxSTC_ERLANG_CHARACTER,{236,155,172}},
	       {?wxSTC_ERLANG_MACRO,    {40,144,170}},
	       {?wxSTC_ERLANG_RECORD,   {40,100,20}},
	       {?wxSTC_ERLANG_SEPARATOR,{0,0,0}},
	       {?wxSTC_ERLANG_NODE_NAME,{0,0,0}},
	       %% Optional 2.9 stuff
	       {?wxSTC_ERLANG_COMMENT_FUNCTION, {160,53,35}},
	       {?wxSTC_ERLANG_COMMENT_MODULE, {160,53,35}},
	       {?wxSTC_ERLANG_COMMENT_DOC, {160,53,35}},
	       {?wxSTC_ERLANG_COMMENT_DOC_MACRO, {160,53,35}},
	       {?wxSTC_ERLANG_ATOM_QUOTED, {0,0,0}},
	       {?wxSTC_ERLANG_MACRO_QUOTED, {40,144,170}},
	       {?wxSTC_ERLANG_RECORD_QUOTED, {40,100,20}},
	       {?wxSTC_ERLANG_NODE_NAME_QUOTED, {0,0,0}},
	       {?wxSTC_ERLANG_BIFS, {255,11,4}},%{130,40,172}
	       {?wxSTC_ERLANG_MODULES, {64,102,244}},
	       {?wxSTC_ERLANG_MODULES_ATT, {64,102,244}}
	      ],
    SetStyle = fun({Style, Color}) ->
		       ?wtc:styleSetForeground(Log, Style, Color)
	       end,
    [SetStyle(Style) || Style <- Styles],
    ?wtc:setKeyWords(Log,1, keyMark()),


%   ?wtc:styleSetForeground(Log,?wxSTC_ERLANG_STRING,{85,85,85}),
%    ?wtc:clearDocumentStyle(Log),
%    case string:find(Str,"EXIT:")  of
%         nomatch ->  ok;
%	 _ ->   ?wtc:styleSetForeground(Log,?wxSTC_ERLANG_STRING,{255,15,15}),
%		?wtc:styleSetForeground(Log,?wxSTC_ERLANG_STRING,{255,15,15}),
%		?wtc:styleSetForeground(Log,?wxSTC_ERLANG_STRING,{255,15,15}),
%		?wtc:styleSetForeground(Log,?wxSTC_ERLANG_STRING,{255,15,15}),
%		?wtc:styleSetForeground(Log,?wxSTC_ERLANG_STRING,{255,15,15}),
%		io:format("EXIT--------: ")    
%    end,			 
%    case string:find(Str,"Error") of
%         nomatch -> ok;
%	 _ ->   ?wtc:styleSetForeground(Log,?wxSTC_ERLANG_STRING,{255,105,105}),
%		io:format("Error--------: ")    
%    end,
%     io:format("OK--------: "),	
%    ?wtc:insertText(Log,0,io_lib:format(Str,Args)),
%    ?wtc:setCaretLineBackground(Log,{0,0,15}),	 
%    ?wtc:insertText(Log,0,io_lib:format(Str,Args)),
    ?wtc:insertText(Log,0,io_lib:format(Str,Args)),
    ?wtc:setKeyWords(Log,1, keyMark()).
%    io:format("Args2: ~p~n",[Args]),
%    io:format("Str2: ~p~n",[Str]).

    


-define(DEBUG_NONE, 101).
-define(DEBUG_VERBOSE, 102).
-define(DEBUG_TRACE, 103).
-define(DEBUG_DRIVER, 104).
    
init(Options) ->
    wx:new(Options),
%    process_flag(trap_exit, true),

    Frame = wxFrame:new(wx:null(), ?wxID_ANY, "Snmp widgets", [{size,{1000,500}}]),
    MB = wxMenuBar:new(),
    File    = wxMenu:new([]),
    wxMenu:append(File, ?wxID_PRINT, "&Print code"),
    wxMenu:appendSeparator(File),
    wxMenu:append(File, ?wxID_EXIT, "&Quit"),
    Debug    = wxMenu:new([]),
    wxMenu:appendRadioItem(Debug, ?DEBUG_NONE, "None"), 
    wxMenu:appendRadioItem(Debug, ?DEBUG_VERBOSE, "Verbose"), 
    wxMenu:appendRadioItem(Debug, ?DEBUG_TRACE, "Trace"), 
    wxMenu:appendRadioItem(Debug, ?DEBUG_DRIVER, "Log"), 
    Help    = wxMenu:new([]),
    wxMenu:append(Help, ?wxID_HELP, "Help"), 
    wxMenu:append(Help, ?wxID_ABOUT, "About"), 
    wxMenuBar:append(MB, File, "&File"),
    wxMenuBar:append(MB, Debug, "&Debug"),
    wxMenuBar:append(MB, Help, "&Help"),
    wxFrame:setMenuBar(Frame,MB),

    wxFrame:connect(Frame, command_menu_selected),
    wxFrame:connect(Frame, close_window),
    


    _SB = wxFrame:createStatusBar(Frame,[]),

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %%   T        Uppersplitter         %%
    %%   O        Left   |    Right     %% 
    %%   P  Widgets|Code |    Pannel    %%
    %%   S  ----------------------------%%
    %%   P          Log Window          %%
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    TopSplitter   = wxSplitterWindow:new(Frame, [{style, ?wxSP_NOBORDER}]),    
    UpperSplitter = wxSplitterWindow:new(TopSplitter, [{style, ?wxSP_NOBORDER}]),    
    wxSplitterWindow:setSashGravity(TopSplitter,   0.5),
    wxSplitterWindow:setSashGravity(UpperSplitter, 0.60),
    Example = fun(Beam) ->
		      "snmpe_" ++ F = filename:rootname(Beam),
		      F
	      end,
    Mods = [Example(F) || F <- filelib:wildcard("snmpe_*.beam")],


%[{style, ?wxLC_REPORT bor ?wxLC_VIRTUAL}]

    IL = wxImageList:new(16,16),
    _Cp=wxImageList:add(IL, wxArtProvider:getBitmap("wxART_COPY", [{size, {16,16}}])),
    _Wi=wxImageList:add(IL, wxArtProvider:getBitmap("wxART_MISSING_IMAGE", [{size, {16,16}}])),
    _Qu=wxImageList:add(IL, wxArtProvider:getBitmap("wxART_QUESTION", [{size, {16,16}}])),
    _Wa=wxImageList:add(IL, wxArtProvider:getBitmap("wxART_WARNING", [{size, {16,16}}])),

% bor ?wxLC_VIRTUAL}
    LC_Opts = [{style, ?wxLC_REPORT}], 

    CreateLB = fun(Parent) ->
%		       wxListBox:new(Parent, ?wxID_ANY, 
%				     [{style, ?wxLB_SINGLE},
%				      {choices, Mods}])
		       wxListCtrl:new(Parent, LC_Opts)
	       end,


    {LBPanel, [LB],_} = create_subwindow(UpperSplitter, "Module", [CreateLB]),

    wxListCtrl:setImageList(LB, IL, ?wxIMAGE_LIST_SMALL),
    wxListCtrl:insertColumn(LB, 0, "Name"),
    wxListCtrl:setColumnWidth(LB, 0, 100),


    Fun=
  	fun(Item) ->
		Count=wxListCtrl:getItemCount(LB),
		wxListCtrl:insertItem(LB,Count, ""),
		wxListCtrl:setItem(LB,Count,0,Item )
   	       end,

    wx:foreach(Fun,Mods),	       

%		wxListCtrl:setItemImage(LB,0,Cp),
%		wxListCtrl:setItemImage(LB,1,Wi),
%		wxListCtrl:setItemImage(LB,2,Qu),
%               wxListCtrl:setItemImage(LB,3,Wa),
    
    wxListCtrl:setItemState(LB, 0,?wxLIST_STATE_SELECTED,?wxLIST_STATE_SELECTED),
    wxListCtrl:getItemState(LB, 0, ?wxLIST_STATE_SELECTED),
    wxListCtrl:connect(LB, command_list_item_selected, []),

    %% Pannel: 
    {DemoPanel, [], DemoSz} = create_subwindow(UpperSplitter, "Work panel", []),
    
    %% UpperSplitter:
    wxSplitterWindow:splitVertically(UpperSplitter, LBPanel,DemoPanel,
				     [{sashPosition,100}]),
    
    %% TopSplitter: 
    AddEvent = fun(Parent) ->
		       EventText =?wtc:new(Parent,[{style,?wxSTC_ERLANG_DEFAULT}]), 
		       FixedFont = wxFont:new(10, ?wxFONTFAMILY_TELETYPE, ?wxNORMAL, ?wxNORMAL,[]),
		       ?wtc:styleClearAll(EventText),
		       ?wtc:styleSetFont(EventText, ?wxSTC_STYLE_DEFAULT, FixedFont),
		       ?wtc:setLexer(EventText, ?wxSTC_LEX_ERLANG),
		       ?wtc:setMarginType(EventText, 0, ?wxSTC_MARGIN_NUMBER),
		       ?wtc:setMarginWidth(EventText, 1, 0),
		       ?wtc:setSelectionMode(EventText, ?wxSTC_SEL_LINES),
 		       ?wtc:appendText(EventText, "Welcome\n"),
%		       ?wtc:styleSetFont(EventText, 255,0}}, FixedFont),
		       ?wtc:styleSetForeground(EventText, ?wxSTC_ERLANG_ATOM, {255,0,0}),
%		       ?wtc:setKeyWords(EventText,57000,keyWords1()),
		       ?wtc:gotoLine(EventText,-1),
		       EventText
	       end,

    {EvPanel, [EvCtrl],_} = create_subwindow(TopSplitter, "Events", [AddEvent]),
    wxSplitterWindow:splitHorizontally(TopSplitter, UpperSplitter, EvPanel, 
				       [{sashPosition,-150}]),
    
    wxFrame:show(Frame),
    State = #state{win=Frame, demo={DemoPanel,DemoSz}, selector=LB, log=EvCtrl },
    %% Load the first example:
    Ex=wxListCtrl:getItemText(LB,0),
%    io:format("Ex1- ~p~n",[Ex1]),
%   Ex1 = wxListBox:getStringSelection(LB),        
    process_flag(trap_exit, true),
    ExampleObj = load_example(Ex, State), 
    wxSizer:add(DemoSz, ExampleObj, [{proportion,1}, {flag, ?wxEXPAND}]),
    wxSizer:layout(DemoSz),
    
    %% The windows should be set up now, Reset Gravity so we get what we want
    wxSplitterWindow:setSashGravity(TopSplitter,   1.0),
    wxSplitterWindow:setSashGravity(UpperSplitter, 0.0),
    wxSplitterWindow:setMinimumPaneSize(TopSplitter, 1),
    wxSplitterWindow:setMinimumPaneSize(UpperSplitter, 1),

    wxToolTip:enable(true),
    wxToolTip:setDelay(800),
%    {ok, _Pid} = snmp_e_manager:start_link(),
    K1=filelib:wildcard("./mibs/*.bin"),
    load_mib(K1,#state{log=EvCtrl}),
%    io:format("Frame: ~p ExampleObj: ~p ~n", [Frame,ExampleObj]),

    {Frame, State#state{example=ExampleObj}}.


create_subwindow(Parent, BoxLabel, Funs) ->
    Panel = wxPanel:new(Parent),
    Sz    = wxStaticBoxSizer:new(?wxVERTICAL, Panel, [{label, BoxLabel}]),
    wxPanel:setSizer(Panel, Sz),
    Ctrls = [Fun(Panel) || Fun <- Funs],
    [wxSizer:add(Sz, Ctrl, [{proportion, 1}, {flag, ?wxEXPAND}])  || Ctrl <- Ctrls],
    {Panel, Ctrls, Sz}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Callbacks
%% Handled as in normal gen_server callbacks
handle_info({'EXIT',_, wx_deleted}, State) ->
    {noreply,State};
handle_info({'EXIT',_, shutdown}, State) ->
    {noreply,State};
handle_info({'EXIT',_, normal}, State) ->
    {noreply,State};
handle_info(Msg, State) ->
    io:format("Got Info-- ~p~n",[Msg]),
    io:format("Got Info-log- ~p~n",[State#state{}]),
    snmpe1:format(State#state{},"Got Info-- ~p\n",[Msg]),
    {noreply,State}.

handle_call(Msg, _From, State) ->
    io:format("Got Call ~p~n",[Msg]),
    {reply,ok,State}.

handle_cast(Msg, State) ->
    io:format("Got cast ~p~n",[Msg]),
    {noreply,State}.

%% Async Events are handled in handle_event as in handle_info
handle_event(#wx{obj = LB, event = #wxList{itemIndex = NEx}},
	     State = #state{demo={_,DemoSz}, example=Example}) ->
             Ex=wxListCtrl:getItemText(LB,NEx),
%    io:format("Ex: ~p~n", [Ex]),
    case Ex of
	[] ->
	    {noreply, State};
	_  ->
	    wxSizer:detach(DemoSz, Example),
	    wx_object:call(Example, shutdown),
	    NewExample = load_example(Ex, State),
	    wxSizer:add(DemoSz, NewExample, [{proportion,2}, {flag, ?wxEXPAND}]),
	    wxSizer:layout(DemoSz),
	    {noreply, State#state{example=NewExample}}
    end;
handle_event(#wx{id = Id,
		 event = #wxCommand{type = command_menu_selected}},
	     State = #state{}) ->
    case Id of
	?wxID_PRINT ->
	    %% If you are going to printout mainly text it is easier if
	    %% you generate HTML code and use a wxHtmlEasyPrint
	    %% instead of using DCs
	    {noreply, State};
	?DEBUG_TRACE ->
	    wx:debug(trace),
	    snmpm:verbosity(net_if,trace),
	    snmpm:verbosity(server,trace),
	    snmpm_config:verbosity(trace),
	    {noreply, State};
	?DEBUG_DRIVER ->
	    wx:debug(driver),
            snmpm:verbosity(net_if,log),
	    snmpm:verbosity(server,log),
	    snmpm_config:verbosity(log),
	    {noreply, State};
	?DEBUG_VERBOSE ->
	    wx:debug(verbose),
            snmpm:verbosity(net_if,debug),
	    snmpm:verbosity(server,debug),
	    snmpm_config:verbosity(debug),
	    {noreply, State};
	?DEBUG_NONE ->
	    wx:debug(none),
            snmpm:verbosity(net_if,silence),
	    snmpm:verbosity(server,silence),
	    snmpm_config:verbosity(silence),
	    {noreply, State};
	?wxID_HELP ->
	    wx_misc:launchDefaultBrowser("http://snmp.ru"),
	    {noreply, State};
	?wxID_ABOUT ->
	    WxWVer = io_lib:format("~p.~p.~p.~p",
				   [?wxMAJOR_VERSION, ?wxMINOR_VERSION,
				    ?wxRELEASE_NUMBER, ?wxSUBRELEASE_NUMBER]),
	    application:load(wx),
	    {ok, WxVsn} = application:get_key(wx,  vsn),
	    AboutString =
		"Demo of various widgets\n"
		"Authors: Alex gog\n\n" ++
		"Frontend: wx-" ++ WxVsn ++
		"\nBackend: wxWidgets-" ++ lists:flatten(WxWVer),

	    wxMessageDialog:showModal(wxMessageDialog:new(State#state.win, AboutString,
							  [{style,
							    ?wxOK bor
							    ?wxICON_INFORMATION bor
							    ?wxSTAY_ON_TOP},
							   {caption, "About"}])),
	    {noreply, State};
	?wxID_EXIT ->
	    wx_object:call(State#state.example, shutdown),
            snmp_e_manager:stop(),
	    {stop, normal, State};
	_ ->
	    {noreply, State}
    end;
handle_event(#wx{event=#wxClose{}}, State = #state{win=Frame}) ->
    io:format("~p Closing window ~n",[self()]),
    ok = wxFrame:setStatusText(Frame, "Closing...",[]),
    {stop, normal, State};
handle_event(Ev,State) ->
    io:format("~p Got event ~p ~n",[?MODULE, Ev]),
    {noreply, State}.

%-------------------------------------

code_change(_, _, State) ->
    {stop, not_yet_implemented, State}.

terminate(_Reason, State = #state{win=Frame}) ->
    catch wx_object:call(State#state.example, shutdown),
    wxFrame:destroy(Frame),
    wx:destroy().

%%%%%%%%%%%%%%%%% Internals %%%%%%%%%%

load_example(Ex, #state{demo={DemoPanel,DemoSz}, log=EvCtrl}) -> 
    ModStr = "snmpe_" ++ Ex,
    Mod = list_to_atom(ModStr),
    Mod:start([{parent, DemoPanel}, {demo_sz, DemoSz}, {log, EvCtrl}]).


%-------------------------------------

load_mib([],_State)->ok;
load_mib([H|T],State)-> 
    case snmpm:load_mib(H) of
    {error,{Note}}-> format([{log,State#state.log}],"Error load MIB: ~p\n'",[Note]);
    {error,{Note,Mib,_}}-> format([{log,State#state.log}],"Error load MIB: ~p' ~p\n",[Note,Mib]);
	_ -> format([{log,State#state.log}],"Load MIB: ~p\n",[H])
    end,
    load_mib(T,State). 
%-------------------------------------

keyMark() ->
    L = ["Error","EXIT:","error","timeout","Error load MIB:"],
    lists:flatten([K ++ " " || K <- L] ++ [0]).

    














