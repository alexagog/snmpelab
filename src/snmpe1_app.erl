%% %CopyrightBegin%
%% 
%% Copyright Ericsson AB 2009-2012. All Rights Reserved.
%% 
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%% 
%% %CopyrightEnd%

%% @docfile "snmpe.hrl"
%% @author A. A. Gog <a.gog@list.ru>
%% @copyright 2020 A. A. Gog

%%%-------------------------------------------------------------------
%% @doc snmpe public API
%% @end
%%%-------------------------------------------------------------------

-module('snmpe1_app').

-behaviour(application).


%% Application callbacks

-export([start/2 ,stop/1,stop/0,entities/0,start_entities/2]).

-include("include/snmp_debug.hrl").

%%====================================================================
%% SNMPE application
%%====================================================================

start(_Type, _Opts) ->
    ?d("start -> entry with"
      "~n   Type. ~p", [Type]),
    %% This is the new snmp application config format
    %% First start the (new) central supervisor,
    {ok, Pid} = snmpe1_sup:start_link(),
%    snmpe1:start(),
    {ok, Pid}.


entities() ->
    entities([manager],[]).

entities([],[]) ->
    ?d("entities -> entry when no entities", []),

    get_env();
    %% Could be old style snmp (agent) application config format
    %% but could also be a skeleton start, which means that neither
    %% agent nor manager config has been specified
    
%    case get_env() of
%	[] ->
%	    %% Skeleton start
%	    ?d("entities -> skeleton start", []),
%	    [];
%	OldConf when is_list(OldConf) ->
%	    ?d("entities -> old style config: ~n~p", [OldConf]),
%	    %% Old style snmp (agent) application config format
%	    Conf = snmpa_app:convert_config(OldConf),
%	    ?d("entities -> converted config: ~n~p", [Conf]),
%	    [{agent, Conf}]
%    end;

entities([], Acc) ->
    ?d("entities -> done", []),
    lists:reverse(Acc);
entities([Ent|Ents], Acc) ->
    ?d("entities -> entry with"
       "~n   Ent: ~p", [Ent]),
    io:format("Ent: ~p~n",[Ent]),
    case application:get_env(snmpe, Ent) of
	{ok, Conf} ->
	    entities(Ents, [{Ent, Conf}|Acc]);
	_ ->
	    io:format("Conf---______----->: ~n"),
	    entities(Ents, Acc)
    end.
	
start_entities(_Type,[]) ->
    ok;

start_entities(Type, [{manager, Opts}|Entities]) ->
    io:format("Env Type: ~p~n ,~p~n",[Opts,Type]),
    case snmpe1:start() of
	ok ->
	    start_entities(Type, Entities);
	Error ->
	    Error
    end;


start_entities(Type, [BadEntity|Entities]) ->
    error_msg("Bad snmp configuration: ~n: ~p", [BadEntity]), 
    start_entities(Type, Entities).



%%--------------------------------------------------------------------
stop(_State) ->
    ok.

stop() ->
    snmpe1_app:stop().


get_env() ->
    Env        = application:get_all_env(snmpe),
    DeleteElem = [included_applications],
    F = fun({Key, _}) -> lists:member(Key, DeleteElem) end,
    lists:dropwhile(F, Env).

%%====================================================================
%% Internal functions
%%====================================================================

                        
error_msg(F, A) ->
    error_logger:error_msg("Logger: ~w: " ++ F ++ "~n", [?MODULE|A]).
