%% %CopyrightBegin%
%% 
%% Copyright Ericsson AB 2009-2012. All Rights Reserved.
%% 
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%% 
%% %CopyrightEnd%

%% @docfile "snmpe.hrl"
%% @author A. A. Gog <a.gog@list.ru>
%% @copyright 2020 A. A. Gog

-module(snmpe_get_mib_list).
  
-behaviour(wx_object).
 
-export([start/1, init/1, terminate/2,  code_change/3, config_ets/3,
	 handle_info/2, handle_call/3, handle_cast/2, handle_event/2, snmp_get/4]).

-include_lib("wx/include/wx.hrl").
-include_lib("stdlib/include/ms_transform.hrl").
-include_lib("./include/snmpe.hrl").

-define(Get,20).
-define(Get_Save,21).
-define(Get_bulk,12).
-define(stc, wxListCtrl).
-define(TABLE_M,snmpm_query_m).
-define(CONF_M,"./conf/querys_m.conf").
-define(Bulk_N,100).


-record(state, 
	{
	  parent,
	  config,
	  command,
	  mib
	 }).

start(Config) ->
    wx_object:start_link(?MODULE, Config, []).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
init(Config) ->
        wx:batch(fun() -> do_init(Config) end).

do_init(Config) ->
    Parent = proplists:get_value(parent, Config),  
    Panel = wxPanel:new(Parent, []),

    %% Setup sizers
    MainSizer = wxBoxSizer:new(?wxVERTICAL),
    
    BConfSizer = wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, 
				 [{label, "Command  line"}]),
    HConfSizer = wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, 
				 [{label, "Host line"}]),
    SzFlags = [{proportion, 0}, {border, 2}, {flag, ?wxALL}],

%-----------------------------------------------------------------
   Ip_addr_Sizer = wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, 
				 [{label, "Ip addr"}]),
   Snmp_ver_Sizer = wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, 
				 [{label, "snmp Ver"}]),
   Community_Sizer = wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, 
				 [{label, "Community"}]),
   Port_Sizer = wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, 
				 [{label, "Port"}]),
   Mib_Sizer = wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, 
				 [{label, "Mib"}]),
   Run_Sizer= wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, 
				 [{label, "Run get"}]),
%-----------------------------------------------------------------

   Sizer3 = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				  [{label, "Snmp results"}]),


%% Create a wxChoice


   {L}=sel:start(mib),

   Ip_Choice = wxTextCtrl:new(Panel, 14, [{value, ?Ip_addr},{size,{150,20}},{style, ?wxDEFAULT}]),
   wxChoice:setToolTip(Ip_Choice, "Ip"),
   Snmp_Ver_Choice = wxChoice:new(Panel, 15, [{size,{50,20}},{choices, ?snmp_ver}]),
   wxChoice:setSelection(Snmp_Ver_Choice,1),
   wxChoice:setToolTip(Snmp_Ver_Choice, "SNMP v."),
   Community_Choice = wxChoice:new(Panel, 16, [{size,{150,20}},{choices, ?snmp_com}, {pos,{0,0}}]),   
   wxChoice:setSelection(Community_Choice,0),
   wxChoice:setToolTip(Community_Choice, "Community"),
   Port_Choice = wxTextCtrl:new(Panel, 17, [{value, ?Port},{size,{70,20}},{style, ?wxDEFAULT}]),
   wxChoice:setToolTip(Port_Choice, "Port"),
   Mib_Choice = wxChoice:new(Panel, 18, [{size,{200,20}},{choices, L}]),
   wxChoice:setToolTip(Mib_Choice, "Mib"),
   wxChoice:setSelection(Mib_Choice,1),
   B_get = wxButton:new(Panel, 20, [{label,"Get bulk"}]),
   wxButton:setToolTip(B_get, "Get snmp data"),
   Oid_Choice = wxTextCtrl:new(Panel, 19, [{value, "[]"},{size,{150,20}},{style, ?wxDEFAULT}]),
   wxChoice:setToolTip(Oid_Choice, "Oid"),
   Save_get = wxButton:new(Panel, 21, [{label,"Save"}]),
   wxButton:setToolTip(Save_get, "Save get single snmp data"),

 
% out results
    ListCtrl3 = ?stc:new(Panel,[{winid,3}, {style, ?wxLC_REPORT}]),


%% Add to sizers 

    wxSizer:add(BConfSizer,Ip_addr_Sizer,[] ),
    wxSizer:add(BConfSizer,Snmp_ver_Sizer, []),
    wxSizer:add(BConfSizer,Community_Sizer, []),
    wxSizer:add(BConfSizer,Port_Sizer, []),
    wxSizer:add(BConfSizer,Mib_Sizer, []),
    wxSizer:add(BConfSizer,Run_Sizer, []),
    wxSizer:add(HConfSizer,Oid_Choice,SzFlags),
    wxSizer:add(HConfSizer,Save_get,SzFlags),
 


    wxSizer:add(Ip_addr_Sizer, Ip_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Snmp_ver_Sizer, Snmp_Ver_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Community_Sizer, Community_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Port_Sizer,Port_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Mib_Sizer, Mib_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Run_Sizer, B_get,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Sizer3, ListCtrl3, [{flag, ?wxEXPAND},{proportion,1}]),
    wxSizer:add(MainSizer, BConfSizer,  []),
    wxSizer:add(MainSizer, HConfSizer,  []),
    wxSizer:add(MainSizer, Sizer3, [{flag, ?wxEXPAND},{proportion,1}]),
    wxPanel:setSizer(Panel, MainSizer),

    wxSizer:layout(MainSizer),
    wxWindow:refresh(Panel),

    wxChoice:connect(Mib_Choice,command_choice_selected),
    wxWindow:connect(Panel, command_button_clicked),
    wxListCtrl:connect(ListCtrl3, command_list_item_selected, []),

    {Panel, #state{parent=Panel, config=Config}}.

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Async Events are handled in handle_event as in handle_info
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%GET button
handle_event((#wx{id=?Get , event=#wxCommand{type=command_button_clicked}}),
	     State=#state{parent=Parent}) -> get_save(get,State,Parent);
%%SAVE button
handle_event((#wx{id=?Get_Save , event=#wxCommand{type=command_button_clicked}}),
	     State = #state{parent=Parent}) -> get_save(save,State,Parent);
%%GET_BULK
handle_event(#wx{id=?Get_bulk, event=#wxCommand{type=command_button_clicked}}, 
    State = #state{parent=Parent}) ->
    B0 = wxWindow:findWindowById(?Get_bulk, [{parent, Parent}]),
    Butt = wx:typeCast(B0, wxButton),
    Label = wxButton:getLabel(Butt),
    snmpe1:format(State#state.config,"Button: \'~ts\' clicked~n",[Label]),
    {noreply,State};

handle_event(#wx{id=Id, event=#wxCommand{type=command_button_clicked}}, 
    State = #state{parent=Parent}) ->
    B0 = wxWindow:findWindowById(Id, [{parent, Parent}]),
    Butt = wx:typeCast(B0, wxButton),
    Label = wxButton:getLabel(Butt),
    snmpe1:format(State#state.config,"Button: \'~ts\' clicked~n",[Label]),
    {noreply,State};

handle_event(#wx{event = #wxCommand{type = command_choice_selected,cmdString = Value}},
    State = #state{parent=Parent}) ->
    snmpe1:format(State#state.config,"Selected MIB ~p\n",[Value]),
    get_save(get,State#state{mib=Value},Parent),
    {noreply, State#state{mib=Value}};

handle_event(#wx{event = #wxList{type = command_list_item_selected, itemIndex = Item}, obj = ListCtrl3},
    State = #state{parent=Parent}) ->
    B1 = wxWindow:findWindowById(19, [{parent, Parent}]),
    SetOid=wx:typeCast(B1,wxTextCtrl),
    ItemTextT = wxListCtrl:getItemText(ListCtrl3, Item),
    wxTextCtrl:setValue(SetOid,ItemTextT),
    snmpe1:format(State#state.config, "You have selected ~p.\n", [ItemTextT]),
    {noreply, State};

%% END event 
handle_event(Ev = #wx{}, State = #state{}) ->
    snmpe1:format(State#state.config,"Got Event ~p\n",[Ev]),
    {noreply, State}.

%% Callbacks handled as normal gen_server callbacks
handle_info(Msg, State) ->
    snmpe1:format(State#state.config, "Got Info ~p\n",[Msg]),
    {noreply, State}.

handle_call(shutdown, _From, State=#state{parent=Panel}) ->
    wxPanel:destroy(Panel),
    {stop, normal, ok, State};

handle_call(Msg, _From, State) ->
    snmpe1:format(State#state.config,"Got Call ~p\n",[Msg]),
    {reply, {error,nyi}, State}.

handle_cast(Msg, State) ->
    io:format("Got cast ~p~n",[Msg]),
    {noreply,State}.

code_change(_, _, State) ->
    {stop, ignore, State}.

terminate(_Reason, _State) ->
    ok.

get_save(_Do,State,Parent)->

    Wx_IP = wxWindow:findWindowById(14, [{parent, Parent}]),
    Obj_IP = wx:typeCast(Wx_IP, wxTextCtrl),
    IP_String = wxTextCtrl:getValue(Obj_IP),
     
    {Res,IP_Name,IP_Addr}= snmp_e_fun:ip_name(IP_String),
   
    Wx_Ver = wxWindow:findWindowById(15, [{parent, Parent}]),
    Obj_Ver = wx:typeCast(Wx_Ver, wxChoice),
    N_Ver = wxChoice:getSelection(Obj_Ver),  
    VerStr=wxChoice:getString(Obj_Ver, N_Ver),
    Ver=force:to_atom(VerStr),

    Wx_Community = wxWindow:findWindowById(16, [{parent, Parent}]),
    Obj_Wx_Community = wx:typeCast(Wx_Community, wxChoice),
    Comm_N = wxChoice:getSelection(Obj_Wx_Community),
    Community=wxChoice:getString(Obj_Wx_Community,Comm_N),

 
    Wx_Port = wxWindow:findWindowById(17, [{parent, Parent}]),
    Obj_Port = wx:typeCast(Wx_Port, wxTextCtrl),
    Port_String = wxTextCtrl:getValue(Obj_Port), 
    Port=force:to_integer(Port_String),
     

    Mib_String= State#state.mib,
    {Oid}=sel:start(oid,Mib_String),
    CtrlOut=wxWindow:findWindowById(3, [{parent, Parent}]),
    ListCtrl3 = wx:typeCast(CtrlOut, ?stc),


%AgentTestConfig =
%    	[{tdomain,snmpUDPDomain},
%	 {reg_type,target_name},
%	 {taddress,{IP,Port}},
%	 {engine_id,"engine_test"},
%	 {timeout, 100000},           % Timeout
%	 {max_message_size, 1000},    % Max message (packet) size
%	 {version, v2 },              % MPModel
%	 {sec_model, Ver},            % SecModel
%	 {sec_name, "initial"},       % SecName
%	 {sec_level, noAuthNoPriv},   % SecLevel
%	 {community, Community}],     % Community

 {ok,ConfDir}=snmpm_config:system_info(config_dir),  
 Hash=erlang:binary_to_list(base64:encode(crypto:strong_rand_bytes(15))),  
 Conf={simple_user,Hash, Community, IP_Addr, Port, IP_Name, infinity, 10000000, v2, Ver, "initial", noAuthNoPriv},
 AppendConf={[ConfDir],Conf},
 {ok,{simple_user,Hash,AgentTestConfig,v2}}=snmpm_config:check_agent_config(Conf),

 snmpm_config:register_agent(simple_user,Hash,AgentTestConfig), 
 wx_misc:beginBusyCursor(),
    snmp_get(State,Hash,Oid,ListCtrl3),
 wx_misc:endBusyCursor(),

  {Label2,RES2}= make_get(Res,_Do,Hash,AppendConf,Oid,State),
  snmpe1:format(State#state.config,"Button: \'~ts\' clicked ~p~n",[Label2,RES2]),
    {noreply,State}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Local functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------
 make_get(St,That,Hash,{ConfDir,Config},Oid,State)->
 {RES,Stat} =   case {St,That} of
	{ok,save} -> {add_agent_conf (ConfDir,Config,Oid,State),"Save agents"};
        {_,save} -> {add_agent_conf (ConfDir,Config,Oid,State),"Not resolve IP address"};
	{_,get} ->  snmpm_config:unregister_agent(simple_user,Hash), 
			{"OK","Get test"}
			
    end,
 {RES,Stat}.

%----------------------------------------------
 snmp_get(State,Agent,Oid,ListCtrl3)->

%  io:format("~n~n~nTt11111 ~p~n~p~n",[Oid,ListCtrl3]),
     case (catch snmp_e_manager:sync_get(Agent,Oid)) of
        {'EXIT',_W} ->?stc:setText(ListCtrl3,io_lib:format("Error EXIT: ~p~n",[_W])),
		     error ;
	{ok,{noError,0,Tesing},_Timer}->
	     {ok,ListOids}=snmpe_test2:out_vbs_io(Tesing,ios),
	     create_list_ctrl(ListCtrl3,ListOids), ok;
	{_,{_T,_,_W},_} ->
	     snmpe1:format(State#state.config,"Out: \'~ts\' ~p~n",[_T]);
	 {error,{_M,_V}} ->  snmpe1:format(State#state.config,"Not find: ~p ~p~n ",[_M,_V])   
      end.


%---------------------------------------------
%% Add agent config file
%---------------------------------------------
 add_agent_conf(ConfigDir,Conf,_Oid1,State) -> 
 Ress1=ets:select(snmpm_agent_table,[{{'$1','$2'},[{'==','$2',{const,{element(4,Conf),element(5,Conf)}}}],['$1']}]),
 case lists:flatlength(Ress1)>0  of
    true -> error ,
	     snmpe1:format(State#state.config,"Allready agent in use: ~p ~n",[Ress1]);
    false  -> snmpm_conf:append_agents_config(ConfigDir,[Conf]),
	     snmpe1:format(State#state.config,"Add agent for use: ~p ~n",[Ress1])
 end, 
 add_query_conf(ConfigDir,Conf,State),
 ok.

%---------------------------------------------
%% Work add to config files
%--------------------------------------------
 add_query_conf(ConfigDir,Conf,State = #state{parent=Parent})->

  B1 = wxWindow:findWindowById(19, [{parent, Parent}]),
  SetOid=wx:typeCast(B1,wxTextCtrl),
  Oid = wxTextCtrl:getValue(SetOid),
  Oid2=snmp_e_fun:wxTCtrl(Oid),
    
  Table=?TABLE_M,
  File_conf=?CONF_M,

  config_ets(ConfigDir, File_conf,Table),
  CountA=ets:select(snmpm_agent_table,[{{'$1','$2'},[{'==','$2',{const,{element(4,Conf),element(5,Conf)}}}],['$1']}]),
   ID= case length(CountA) >=2  of
	   false -> snmpm_conf:append_agents_config(ConfigDir,[Conf]),
		 element(2,Conf);   
	   true -> 
	       FindA=lists:last(CountA),
	       FinID =case element(2,Conf) == element(1,FindA) of 
		       true -> CountA;
	     	       false ->	lists:reverse(CountA)
	       end,
		   [{H,_}|_T] = FinID,	     
		   snmpm_config:unregister_agent(element(1,Conf),element(2,Conf)),
		   H
        end,  

    IP=element(4,Conf),
    Port=element(5,Conf),
    NameIP=element(6,Conf),

    Conf_txt=io_lib:format("{~p, 0, 1, ~p, ~p, ~p, ~p}.\n",[ID,Oid2,IP,Port,NameIP]),

 case ets:select(?TABLE_M,[{{'$1','$2','$3','$4','$5','$6','$7'},[{'==','$5',{const,IP}},{'==','$4',{const,Oid2}}],['$$']}])==[] of
      true -> 
	 file:write_file(?CONF_M,Conf_txt,[append]),
	 Conf_q={ID, 0,0,Oid2,IP,Port,NameIP},
	 ets:insert_new(Table,Conf_q);
      false -> snmpe1:format(State#state.config,"IP Oid allready in query use : ~p , ~p ",[IP,Oid2])
 end,  
    ok.

%---------------------------------------------
%% Laad config to ets table if not load
%---------------------------------------------
config_ets (ConfigDir,File_conf,Table) ->
  case ets:info(Table,name)==Table of
    true ->  ok;
    false -> ets:new(Table, [bag,public,named_table]),
	     snmpe_ets:read_config_file(ConfigDir,File_conf,Table)
  end.


create_list_ctrl(Win,Rows) ->

    ?stc:clearAll(Win),
    ?stc:insertColumn(Win, 0, "Oid", []),
    ?stc:insertColumn(Win, 1, "Type", []),
    ?stc:insertColumn(Win, 2, "Value", []),

    ?stc:setColumnWidth(Win, 0, 200),
    ?stc:setColumnWidth(Win, 1, 200),
    ?stc:setColumnWidth(Win, 2, 400),
 
    Fun =
	fun(Int) ->
                {Oid,Type,Val}=Int,
   	 case Type == 'NULL' of
           false ->Count=?stc:getItemCount(Win),
		   ?stc:insertItem(Win,Count, ""),
              case Count rem 2 of
                0 -> ?stc:setItemBackgroundColour(Win, Count, {240,240,240,255});
	        _ -> ok  
	      end,    
	     Oidp=io_lib:format("~p",[Oid]), 
	     Str_Oid=lists:flatten(Oidp),
	     Typep=io_lib:format("~p",[Type]), 
	     Str_Type=lists:flatten(Typep),		
	     Valp=io_lib:format("~p",[Val]), 
	     Str_Val=lists:flatten(Valp),
	     ?stc:setItem(Win, Count, 0, Str_Oid),
	     ?stc:setItem(Win, Count, 1, Str_Type),
	     ?stc:setItem(Win, Count, 2, Str_Val);
	   true-> null	   
	 end	       
       end,
   wx:foreach(Fun, Rows),
   {ok}.
