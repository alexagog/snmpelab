%% %CopyrightBegin%
%% 
%% Copyright Ericsson AB 2009-2012. All Rights Reserved.
%% 
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%% 
%% %CopyrightEnd%

%% @docfile "snmpe.hrl"
%% @author A. A. Gog <a.gog@list.ru>
%% @copyright 2020 A. A. Gog

-module(snmp_e_test2).

-behaviour(wx_object).

-export([start/1, init/1, terminate/2,  code_change/3, config_ets/3,
	 handle_info/2, handle_call/3, handle_cast/2, handle_event/2, snmp_get/3]).

-include_lib("wx/include/wx.hrl").
-include_lib("stdlib/include/ms_transform.hrl").

-define(Get,10).
-define(Get_Save,11).
-define(Get_bulk,12).
-define(stc, wxStyledTextCtrl).
-define(TABLE_M,snmpm_query_m).
-define(CONF_M,"./conf/querys_m.conf").
%-define(Dir_cfg,{ok,ConfDir}=snmpm_config:system_info(config_dir)).

-record(state, 
	{
	  parent,
	  config,
	  command
	 }).

start(Config) ->
    wx_object:start_link(?MODULE, Config, []).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
init(Config) ->
        wx:batch(fun() -> do_init(Config) end).

do_init(Config) ->
    Parent = proplists:get_value(parent, Config),  
    Panel = wxPanel:new(Parent, []),

    %% Setup sizers
    MainSizer = wxBoxSizer:new(?wxVERTICAL),
    Sizer = wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, 
				 [{label, "Command single line"}]),
   Ip_addr_Sizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				 [{label, "Ip addr"}]),
   Snmp_ver_Sizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				 [{label, "snmp Ver"}]),
   Community_Sizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				 [{label, "Community"}]),
   Port_Sizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				 [{label, "Port"}]),
   Oid_Sizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				 [{label, "Oid"}]),
   Run_Sizer= wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				 [{label, "Run test"}]),

   Save_Sizer= wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				 [{label, "Save to file"}]),


    B_get = wxButton:new(Panel, 10, [{label,"Get"}]),
    wxButton:setToolTip(B_get, "Get snmp data"),

    Save_get = wxButton:new(Panel, 11, [{label,"Save"}]),
    wxButton:setToolTip(Save_get, "Save get single snmp data"),

    
%    B_get_next = wxButton:new(Panel, 11, [{label,"Get-next"}]),
%    wxButton:setToolTip(B_get_next, "Get next snmp data"),

%    B_get_bulk = wxButton:new(Panel, 12, [{label,"Get-bulk"}]),
%    wxButton:setToolTip(B_get_bulk, "Get multiple snmp data"),


%    Sizer2 = wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, 
%				  [{label, "Snmp command"}]),
    Sizer3 = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				  [{label, "Snmp results"}]),

%      TextCtrl=snmpe_conf:conf_area(Panel),

%% Create a wxChoice

    Choices_snmp = ["v1","v2c"],

   Ip_Choice = wxTextCtrl:new(Panel, 14, [{value, "127.0.0.1"},{size,{150,20}},{style, ?wxDEFAULT}]),
   wxChoice:setToolTip(Ip_Choice, "Ip"),
   Snmp_Ver_Choice = wxChoice:new(Panel, 15, [{size,{50,20}},{choices, Choices_snmp}, {pos,{1,2}}]),
   wxChoice:setSelection(Snmp_Ver_Choice,1),
   wxChoice:setToolTip(Snmp_Ver_Choice, "SNMP v."),
   Community_Choice = wxTextCtrl:new(Panel, 16, [{value, "bublic"},{size,{150,20}},{style, ?wxDEFAULT}]),
   wxChoice:setToolTip(Community_Choice, "Community"),
%   Port_Choice = wxStyledTextCtrl:new(Panel, [{size,{70,20}},{id,17}]),
    Port_Choice = wxTextCtrl:new(Panel, 17, [{value, "161"},{size,{70,20}},{style, ?wxDEFAULT}]),
   wxChoice:setToolTip(Community_Choice, "Port"),
   Oid_Choice = wxTextCtrl:new(Panel, 18, [{value, "1.3.6"},{size,{200,20}},{style, ?wxDEFAULT}]),
   wxChoice:setToolTip(Community_Choice, "Oid's"),
 
%    wxChoice:setToolTip(Choice, "A wxChoice"),

%    TextCtrl  = ?stc:new(Panel,[{id,1},{size,{5,50}}]),

%    ?stc:styleClearAll(TextCtrl),
%    FixedFont = wxFont:new(10, ?wxFONTFAMILY_TELETYPE, ?wxNORMAL, ?wxNORMAL,[]),
%    ?stc:styleSetFont(TextCtrl, ?wxSTC_STYLE_DEFAULT, FixedFont),
%    ?stc:setLexer(TextCtrl, ?wxSTC_LEX_ERLANG),
%    ?stc:setMarginType(TextCtrl, 0, ?wxSTC_MARGIN_NUMBER),
%    LW = ?stc:textWidth(TextCtrl, ?wxSTC_STYLE_LINENUMBER, "9"),
%    ?stc:setMarginWidth(TextCtrl, 0, LW),
%    ?stc:setMarginWidth(TextCtrl, 1, 0),

%    %% Scrolling
%    Policy = ?wxSTC_CARET_SLOP bor ?wxSTC_CARET_JUMPS bor ?wxSTC_CARET_EVEN, 
%    ?stc:setYCaretPolicy(TextCtrl, Policy, 3),
%    ?stc:setVisiblePolicy(TextCtrl, Policy, 3),
%    ?stc:setReadOnly(TextCtrl, false),
%    Out="agent_103,0,1,1.3.6.1.4.1",
%io_lib:format('~p',[{"agent_103",0,0,[1,3,6,1,4,1]}]),
%    ?stc:setTextRaw(TextCtrl,list_to_binary(Out)),   

    TextCtrl3 = ?stc:new(Panel,[{id,3}]),
    ?stc:styleClearAll(TextCtrl3),
    ?stc:setLexer(TextCtrl3, ?wxSTC_LEX_ERLANG),

    wxWindow:connect(Panel, command_button_clicked),

    %% Add to sizers
    wxSizer:add(Sizer,Ip_addr_Sizer, []),
    wxSizer:add(Sizer,Snmp_ver_Sizer, []),
    wxSizer:add(Sizer,Community_Sizer, []),
    wxSizer:add(Sizer,Port_Sizer, []),
    wxSizer:add(Sizer,Oid_Sizer, []),
    wxSizer:add(Sizer,Run_Sizer, []),
    wxSizer:add(Sizer,Save_Sizer, []),

    wxSizer:add(Ip_addr_Sizer, Ip_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Snmp_ver_Sizer, Snmp_Ver_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Community_Sizer, Community_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Port_Sizer,Port_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Oid_Sizer, Oid_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Run_Sizer, B_get,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Save_Sizer, Save_get,  [{flag, ?wxEXPAND}]),

%    wxSizer:add(Sizer,B_get, []),
%    wxSizer:add(Sizer2,B_get_next, []),
%    wxSizer:add(Sizer2,B_get_bulk, []),

    wxSizer:add(Sizer3, TextCtrl3, [{flag, ?wxEXPAND}, {proportion, 1}]),

    wxSizer:add(MainSizer, Sizer,  [{flag, ?wxEXPAND}]),
%    wxSizer:addSpacer(MainSizer, 1),
%    wxSizer:add(MainSizer, Sizer2, [{flag, ?wxEXPAND}]),
%    wxSizer:addSpacer(MainSizer, 1),
    wxSizer:add(MainSizer, Sizer3, [{flag, ?wxEXPAND}, {proportion, 1}]),

    wxPanel:setSizer(Panel, MainSizer),
    {Panel, #state{parent=Panel, config=Config}}.

 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Async Events are handled in handle_event as in handle_info
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%GET
handle_event((#wx{id=?Get , event=#wxCommand{type=command_button_clicked}}),
	     State=#state{parent=Parent}) -> get_save(get,State,Parent);
%%SAVE
handle_event((#wx{id=?Get_Save , event=#wxCommand{type=command_button_clicked}}),
	     State = #state{parent=Parent}) -> get_save(save,State,Parent);
%%GET_BULK
handle_event(#wx{id=?Get_bulk, event=#wxCommand{type=command_button_clicked}}, 
    State = #state{parent=Parent}) ->
    B0 = wxWindow:findWindowById(?Get_bulk, [{parent, Parent}]),
    Butt = wx:typeCast(B0, wxButton),
    Label = wxButton:getLabel(Butt),
    snmpe1:format(State#state.config,"Button: \'~ts\' clicked~n",[Label]),
    {noreply,State};


handle_event(#wx{id=Id, event=#wxCommand{type=command_button_clicked}}, 
    State = #state{parent=Parent}) ->
    B0 = wxWindow:findWindowById(Id, [{parent, Parent}]),
    Butt = wx:typeCast(B0, wxButton),
    Label = wxButton:getLabel(Butt),
    snmpe1:format(State#state.config,"Button: \'~ts\' clicked~n",[Label]),
    {noreply,State};

handle_event(Ev = #wx{}, State = #state{}) ->
    snmpe1:format(State#state.config,"Got Event ~p\n",[Ev]),
    {noreply, State}.

%% Callbacks handled as normal gen_server callbacks
handle_info(Msg, State) ->
    snmpe1:format(State#state.config, "Got Info ~p\n",[Msg]),
    {noreply, State}.

handle_call(shutdown, _From, State=#state{parent=Panel}) ->
    wxPanel:destroy(Panel),
    {stop, normal, ok, State};

handle_call(Msg, _From, State) ->
    snmpe1:format(State#state.config,"Got Call ~p\n",[Msg]),
    {reply, {error,nyi}, State}.

handle_cast(Msg, State) ->
    io:format("Got cast ~p~n",[Msg]),
    {noreply,State}.

code_change(_, _, State) ->
    {stop, ignore, State}.

terminate(_Reason, _State) ->
    ok.

get_save(_Do,State,Parent)->
    Wx_IP = wxWindow:findWindowById(14, [{parent, Parent}]),
    Obj_IP = wx:typeCast(Wx_IP, wxTextCtrl),
    IP_String = wxTextCtrl:getValue(Obj_IP),
     
    {Res,IP_Name,IP_Addr}= snmp_e_fun:ip_name(IP_String),
   
%    IP2={10,241,7,152},
%    IP=lists:map(fun(X) -> {Int,_} =string:to_integer(X), Int end, string:tokens(IP_String, ".")),

    Wx_Ver = wxWindow:findWindowById(15, [{parent, Parent}]),
    Obj_Ver = wx:typeCast(Wx_Ver, wxChoice),
    N_Ver = wxChoice:getSelection(Obj_Ver),   

    Ver=vers(N_Ver),
    

    Wx_Community = wxWindow:findWindowById(16, [{parent, Parent}]),
    Obj_Wx_Community = wx:typeCast(Wx_Community, wxTextCtrl),
    Community = wxTextCtrl:getValue(Obj_Wx_Community), 
 
    Wx_Port = wxWindow:findWindowById(17, [{parent, Parent}]),
    Obj_Port = wx:typeCast(Wx_Port, wxTextCtrl),
    Port_String = wxTextCtrl:getValue(Obj_Port), 
    Port=force:to_integer(Port_String),
     
    Wx_Oid = wxWindow:findWindowById(18, [{parent, Parent}]),
    Obj_Oid = wx:typeCast(Wx_Oid, wxTextCtrl),
    Oid_String = wxTextCtrl:getValue(Obj_Oid), 
    Oid=lists:map(fun(X) -> {Int,_} =string:to_integer(X), Int end, string:tokens(Oid_String, ".")),

    
    CtrlOut=wxWindow:findWindowById(3, [{parent, Parent}]),
    TextCtrl3 = wx:typeCast(CtrlOut, ?stc),

%AgentTestConfig =
%    	[{tdomain,snmpUDPDomain},
%	 {reg_type,target_name},
%	 {taddress,{IP,Port}},
%	 {engine_id,"engine_test"},
%	 {timeout, 100000},           % Timeout
%	 {max_message_size, 1000},    % Max message (packet) size
%	 {version, v2 },              % MPModel
%	 {sec_model, Ver},            % SecModel
%	 {sec_name, "initial"},       % SecName
%	 {sec_level, noAuthNoPriv},   % SecLevel
%	 {community, Community}],     % Community

 {ok,ConfDir}=snmpm_config:system_info(config_dir),  

 Hash=erlang:binary_to_list(base64:encode(crypto:strong_rand_bytes(15))),  

 Conf={simple_user,Hash, Community, IP_Addr, Port, IP_Name, infinity, 1000, v2, Ver, "initial", noAuthNoPriv},
 
 AppendConf={[ConfDir],Conf},
 
 {ok,{simple_user,Hash,AgentTestConfig,v2}}=snmpm_config:check_agent_config(Conf),

% io:format("AgentTestConfig:~n~p~n,Hash:~p~n",[AgentTestConfig,Hash]),
 snmpm_config:register_agent(simple_user,Hash,AgentTestConfig), 

 wx_misc:beginBusyCursor(),
  snmp_get(Hash,[Oid],TextCtrl3),
 wx_misc:endBusyCursor(),

% io:format("Res ------------------------------------: ~p~n",[Res]),
  
  {Label2,RES2}= make_get(Res,_Do,Hash,AppendConf,Oid,State),
    snmpe1:format(State#state.config,"Button: \'~ts\' clicked ~p~n",[Label2,RES2]),
    {noreply,State}.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Local functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 vers(N)->
    case N of
	1 -> v2c;
	0 -> v1
%	0 -> v1
    end.
%-----------------------------------------------
 make_get(St,That,Hash,{ConfDir,Config},Oid,State)->
 {RES,Stat} =   case {St,That} of
	{ok,save} -> {add_agent_conf (ConfDir,Config,Oid,State),"Save agents"};
        {_,save} -> {"Error","Not save to agents"};
	{_,get} ->  snmpm_config:unregister_agent(simple_user,Hash), 
			{"OK","Get test"}
			
    end,
 {RES,Stat}.

%----------------------------------------------
 snmp_get(Agent,[Oid],TextCtrl3)->
     case (catch snmp_e_manager:sync_get_bulk(Agent,0,100,[Oid])) of

        {'EXIT',_W} ->?stc:setText(TextCtrl3,io_lib:format("Error EXIT: ~p~n",[_W])),
		     error ;
	{ok,Tesing,_Timer}->
	     ?stc:clearAll(TextCtrl3),
             {ok,ConfigDir}=snmpm_config:system_info(config_dir),
             config_ets(ConfigDir,?CONF_M,?TABLE_M ),
	     snmp_e_fun:out_vbs(Tesing,test,TextCtrl3),  
%	     ?stc:setText(TextCtrl3,io_lib:format("Testing:~p~n Timer=~p~n",[Tesing,_Timer])),
		     ok;
	{_,_W} ->?stc:setText(TextCtrl3,io_lib:format("Error: ~p~n",[_W])),
		     error 
 
    end.
%---------------------------------------------
%% Add agent config file
%---------------------------------------------
 add_agent_conf (ConfigDir,Conf,Oid1,State) -> 
% Conf= {simple_user,Hash, Community, IP_Addr, Port, IP_Name, infinity, 1000, v2, Ver, "initial", noAuthNoPriv}
% Taddress=[{element(4,Conf),element(5,Conf)}],
% Fn = ets:fun2ms(fun({M,N}) when N == Taddress ->M end),
 Ress1=ets:select(snmpm_agent_table,[{{'$1','$2'},[{'==','$2',{const,{element(4,Conf),element(5,Conf)}}}],['$1']}]),
 case lists:flatlength(Ress1)>0  of
    true -> error ,
	     snmpe1:format(State#state.config,"Allready agent in use: ~p ~n",[Ress1]);
    false  -> snmpm_conf:append_agents_config(ConfigDir,[Conf]),
	     snmpe1:format(State#state.config,"Add agent for use: ~p ~n",[Ress1])
 end, 
 add_query_conf(ConfigDir,Conf,Oid1,State),
 ok.

%---------------------------------------------
%% Work add to config files
%--------------------------------------------
 add_query_conf(ConfigDir,Conf,Oid2,State)->
%% recreate table snmpm_query_m
% io:format("Conf---: ~p~n", [Conf]),
  Table=?TABLE_M,
  File_conf=?CONF_M,

  config_ets(ConfigDir, File_conf,Table),

   ID= case ets:select(snmpm_agent_table,[{{'$1','$2'},[{'==','$2',{const,{element(4,Conf),element(5,Conf)}}}],['$1']}]) of
	   [] -> element(2,Conf);  
	   [{H,_}|_T] -> H
%  	  [_H|T] -> {IDx,_}=lists:last(T),
%		     IDx	  
        end,  
%   io:format("Conf--ID: ~p~n", [ID]),

%    ID=element(2,Conf),
    IP=element(4,Conf),
    Port=element(5,Conf),
    NameIP=element(6,Conf),

    Conf_txt=io_lib:format("{~p, 0, 1, ~p, ~p, ~p, ~p}.\n",[ID,Oid2,IP,Port,NameIP]),

%    DirConf=string:concat(ConfigDir,?CONF_M),
%    io:format ("DirConf::~p~n",[DirConf]),
 % ets snmpm_query_m {"/ic2huC3lxTx2O3DrZjX",0,1,[1,3,6],{10,241,7,149},161,"host fqdn"}
 case ets:select(snmpm_query_m,[{{'$1','$2','$3','$4','$5','$6','$7'},[{'==','$5',{const,IP}},{'==','$4',{const,Oid2}}],['$$']}])==[] of
      true -> 
%         io:format("DIR=~p~n",[DirConf]),
	 file:write_file(?CONF_M,Conf_txt,[append]),
	 Conf_q={ID, 0,0,Oid2,IP,Port,NameIP},
%	 io:format("~nConf_q: ~p~n",[Conf_q]),
	 ets:insert_new(Table,Conf_q);
      false -> snmpe1:format(State#state.config,"IP Oid allready in query use : ~p , ~p ",[IP,Oid2])
 end,  
    ok.
%---------------------------------------------
%% Laad config to ets table if not load
%---------------------------------------------
config_ets (ConfigDir,File_conf,Table) ->
  case ets:info(Table,name)==Table of
    true ->  ok;
%    io:format("true: ~p~n", [Table]);
    false -> ets:new(Table, [bag,public,named_table]),
	     snmpe_ets:read_config_file(ConfigDir,File_conf,Table)
  end.
%---------------------------------------------
%---------------------------------------------

