%% %CopyrightBegin%
%% 
%% Copyright Ericsson AB 2009-2012. All Rights Reserved.
%% 
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%% 
%% %CopyrightEnd%

%% @docfile "snmpe.hrl"
%% @author A. A. Gog <a.gog@list.ru>
%% @copyright 2020 A. A. Gog

-module(snmp_e_fun).

-export([out_vbs/3, out_vbs_t/5, out_vbs_io/2,wxTCtrl/1,ip_name/1]).

-include_lib("./include/snmp_types.hrl").
-define(stt, wxTreeCtrl).


%--out-win--------------------------------------------------
out_vbs({noError, 0, Vbs},test,WinOut) ->
    (catch out_vbs(Vbs,test,"",WinOut));
out_vbs (Res,_,_WinOut) ->
    {error, {unexpected_result, Res}}.
out_vbs([],test,Acc,_WOut) -> 
    {ok, Acc};

out_vbs([Vb|T],test,_Acc,WOut) ->
    Val=out_vb(Vb),
    {Oid,Type,Val_t} = Val,
    Acc2 = lists:flatten(io_lib:format("~p", [Val])),
    N=wxListCtrl:getItemCount(WOut),
    wxListCtrl:insertItem(WOut,N,""),
    case N rem 2 of
         0 -> wxListCtrl:setItemBackgroundColour(WOut, N, {240,240,240,255});
          _ -> ok  
    end,  
    wxListCtrl:setItem(WOut, N, 0, io_lib:format("~p",[Oid])),
    wxListCtrl:setItem(WOut, N, 1, io_lib:format("~p",[Type])),
    wxListCtrl:setItem(WOut, N, 2, io_lib:format("~p",[Val_t])),

    out_vbs(T,test,Acc2,WOut),
Val.

out_vb(#varbind{oid = Oid, variabletype = Type, value = Val_i}) -> 
      Val = case Type=='OCTET STRING' of
	  true ->   case io_lib:printable_list(Val_i) of
		        false ->  lists:reverse(tl(lists:reverse(Val_i))); 
	 	        _-> Val_i
		    end;
	  _ -> Val_i
      end,
{Oid,Type,Val}.


%-out-tree----------------------------------------
out_vbs_t({noError, 0, Vbs},test,WinOut,Root,_OidR) ->
    (catch out_vbs_t(Vbs,test,"",WinOut,Root,_OidR));
out_vbs_t (Res,_,_WinOut,_Root,_OidR) ->
    {error, {unexpected_result, Res}}.

out_vbs_t([],test,Acc,_WOut,_Root,_OidR) ->
    {ok, Acc};
out_vbs_t([Vb|T],test,_Acc,WOut,Root,OidR) ->
    Val  = out_vb_t(Vb),
    {OidT,_TypeT,_ValT}=Val,
    Val2 = lists:flatten(io_lib:format("~w|~p|~p", [OidT,_TypeT,_ValT])),
    {OidT,_TypeT,_ValT}=Val,
    NextC=case lists:last(OidT)==1  of
	true-> ?stt:appendItem(WOut, Root, Val2);
	false -> Root
    end,
    NextT=?stt:appendItem(WOut, NextC, Val2),
    ?stt:expand(WOut, NextC),
    NextN =case lists:last(OidT)==0 of
	true ->  Root;
	false -> case lists:last(OidT)==1 of
		     true ->NextC;		  
		     false ->Root
		end		
    end,	     
    case lists:prefix(OidR,OidT)  of
	true ->  Root;
	false -> NextT
    end,
    out_vbs_t(T,test,Val2,WOut,NextN,OidT),
Val.

%-out-tree----------------------------------------------------------
out_vb_t(#varbind{oid = Oid, variabletype = Type, value = Val_i}) -> 
%      io:format("~p, ~p, ~p~n",[Oid,Type,Val_i]),
      Val = case Type=='OCTET STRING' of
	  true ->   case io_lib:printable_list(Val_i) of
		        false ->  lists:reverse(tl(lists:reverse(Val_i))); 
	 	        _-> Val_i
		    end;
	  _ -> Val_i
      end,
{Oid,Type,Val}.

%%%-----------------------------------------------------------------------------------
%% Out that all varbinds have the expected name and type
%%%-----------------------------------------------------------------------------------

 out_vbs_io(Vbs,ios)->
    (catch out_vbs_io(Vbs,ios,[])).
 out_vbs_io([],ios,Acc) -> 
    AccR=lists:reverse(Acc),
    {ok, AccR};
 out_vbs_io([Vb|T],ios,Acc) ->
    Val  = out_vb(Vb),
 out_vbs_io(T,ios,[Val|Acc]).

%% Snmpe fun %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%text wxTextCtrl "[1,2,3]" or "[1.2.3]" -> [1,2,3,]
wxTCtrl(Oid_String)->
 case string:str(Oid_String,".") > 0  of
      true -> lists:map(fun(X) -> {Int,_} =string:to_integer(X), Int end, string:tokens(Oid_String, "."));
      false -> A=lists:map(fun(X) -> {Int,_} =string:to_integer(X), Int end, string:tokens(Oid_String, ",")),
        A2=lists:delete(error,A),
        lists:append([1],A2)
 end.

%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 ip_name(IP_Str) ->
    case inet_parse:address(IP_Str) of
    {ok,IP} ->ok,
       case inet:gethostbyaddr(IP) of
          {ok,{_,Name,_,_,_,[IP_g]}}-> {ok,Name,IP_g};
           {error,_} -> {ok,Ipaddr}=inet_parse:address(IP_Str),
           {error,IP_Str,Ipaddr}
       end;   
    {error,_}-> 
	    case inet:getaddr(IP_Str,inet) of
		{ok,IP_g}->{ok,IP_Str,IP_g};
                {error,_}->{error,IP_Str,{127,0,0,1}}
	    end 
    end.
