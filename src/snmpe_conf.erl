%% %CopyrightBegin%
%% 
%% Copyright Ericsson AB 2009-2012. All Rights Reserved.
%% 
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%% 
%% %CopyrightEnd%

%% @docfile "snmpe.hrl"
%% @author A. A. Gog <a.gog@list.ru>
%% @copyright 2020 A. A. Gog


-module(snmpe_conf).
-author('a.gog@list.ru').

-compile(exportall).

-behaviour(wx_object).

-export([start/1, init/1, terminate/2,  code_change/3, 
        handle_info/2, handle_call/3, handle_cast/2, handle_event/2]). 

%,load_conf/2,unload_conf/1 ]).



-include_lib("wx/include/wx.hrl").

-define(ABOUT, (?wxID_ABOUT)).
-define(EXIT, (?wxID_EXIT)).
-define(GET, 200).
-define(OPEN_FILE,(?wxID_OPEN)).
-define(CLOSE_FILE,(?wxID_CLOSE)).
-define(SAVE_FILE, (?wxID_SAVE)).

-define(stc, wxStyledTextCtrl).


-define(conf_agents,"conf/agents.conf").
-define(conf_users,"conf/users.conf").
-define(conf_manager,"conf/manager.conf").
-define(conf_querys_m,"conf/querys_m.conf").


%% For wx-2.9 usage
-ifndef(wxSTC_ERLANG_COMMENT_FUNCTION).
-define(wxSTC_ERLANG_COMMENT_FUNCTION, 14).
-define(wxSTC_ERLANG_COMMENT_MODULE, 15).
-define(wxSTC_ERLANG_COMMENT_DOC, 16).
-define(wxSTC_ERLANG_COMMENT_DOC_MACRO, 17).
-define(wxSTC_ERLANG_ATOM_QUOTED, 18).
-define(wxSTC_ERLANG_MACRO_QUOTED, 19).
-define(wxSTC_ERLANG_RECORD_QUOTED, 20).
-define(wxSTC_ERLANG_NODE_NAME_QUOTED, 21).
-define(wxSTC_ERLANG_BIFS, 22).
-define(wxSTC_ERLANG_MODULES, 23).
-define(wxSTC_ERLANG_MODULES_ATT, 24).
-endif.

%% Основная функция: запускает wx-server, создаёт графические объекты,
%% выводит на экран приложения и обрабатывает запрос на завершение
%% приложения и проводит очистку ресурсов.

%%return record

-record(state, 
	{
	  parent,
	  config,
	  notebook,
          textctrl1, % agents
	  textctrl2, % users
	  textctrl3, % manager
	  textctrl4  % query
	 }).

start(Config) ->
    wx_object:start_link(?MODULE, Config, []).


init(Config) ->
    wx:batch(fun() -> do_init(Config) end).


% Main init

do_init(Config) ->
    Parent = proplists:get_value(parent, Config),  
    Panel = wxScrolledWindow:new(Parent),


    %% Setup sizers
    MainSizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel), 
%				     [{label, "Configure"}]),

    %% Buttons
%    Sz=wxBoxSizer:new(?wxVERTICAL),

%    ButtSz = wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, 
%				  [{label, "Operate command"}]),

    SzFlags = [{proportion, 0}, {border, 0}, {flag, ?wxALL bor  ?wxEXPAND}],
    Expand  = [{proportion, 1}, {border, 0}, {flag, ?wxALL bor  ?wxEXPAND}],

%% Stock Buttons


    Notebook = wxNotebook:new(Panel, 1, [{style, ?wxBK_DEFAULT%,
					        %?wxBK_ALIGN_MASK,
					        %?wxBK_TOP,
					        %?wxBK_BOTTOM,
					        %?wxBK_LEFT,
					        %?wxBK_RIGHT,
					        %?wxNB_MULTILINE % windows only
					 }]),

%Agent.conf

     Win_conf1 = wxPanel:new(Notebook,[]),
     TextCtrl1 =conf_area(Win_conf1),
     ConfFile1= file:read_file(?conf_agents),
     load_conf(TextCtrl1,ConfFile1),

    SizerA = wxBoxSizer:new(?wxVERTICAL),
    wxSizer:add(SizerA,TextCtrl1,[{proportion, 1},{flag, ?wxEXPAND}]),
    wxPanel:setSizer(Win_conf1, SizerA),
    wxNotebook:addPage(Notebook, Win_conf1, "agents", []),

%user.conf

    Win_conf2 = wxPanel:new(Notebook, []),
    TextCtrl2 =conf_area(Win_conf2),
    ConfFile2= file:read_file(?conf_users),
    load_conf(TextCtrl2,ConfFile2),

    SizerB = wxBoxSizer:new(?wxVERTICAL),
    wxSizer:add(SizerB,TextCtrl2,[{proportion, 2},{flag, ?wxEXPAND}]),
    wxPanel:setSizer(Win_conf2, SizerB),
    wxNotebook:addPage(Notebook, Win_conf2, "users", []),

%manger.conf

    Win_conf3 = wxPanel:new(Notebook, []),
    TextCtrl3 =conf_area(Win_conf3),
    ConfFile3= file:read_file(?conf_manager),
    load_conf(TextCtrl3,ConfFile3),

    SizerC = wxBoxSizer:new(?wxVERTICAL),
    wxSizer:add(SizerC,TextCtrl3,[{proportion, 3},{flag, ?wxEXPAND}]),
    wxPanel:setSizer(Win_conf3, SizerC),
    wxNotebook:addPage(Notebook, Win_conf3, "manager", []),

%query_m.conf

    Win_conf4 = wxPanel:new(Notebook, []),
    TextCtrl4 =conf_area(Win_conf4),
    ConfFile4= file:read_file(?conf_querys_m),
    load_conf(TextCtrl4,ConfFile4),

    SizerD = wxBoxSizer:new(?wxVERTICAL),
    wxSizer:add(SizerD,TextCtrl4,[{proportion, 4},{flag, ?wxEXPAND}]),
    wxPanel:setSizer(Win_conf4, SizerD),
    wxNotebook:addPage(Notebook, Win_conf4, "query", []),


    %% Add to sizers

    [wxSizer:add(MainSizer, Items, Flag) || {Items, Flag} <- [{Notebook,Expand}]],


    B_save = wxButton:new(Panel, ?SAVE_FILE, [{label,"Save"}]),
    wxButton:setToolTip(B_save, "Save all configureNormal"),
    [wxSizer:add(MainSizer,Butt,SzFlags) || Butt<-[B_save]],
%    [wxSizer:add(Sz, Button, Flag) || {Button, Flag} <- [{MainSizer ,Expand}]],
    wxWindow:connect(Panel, command_button_clicked),



    wxWindow:connect(Panel, command_button_clicked),
    wxNotebook:connect(Notebook, command_notebook_page_changed,
		       [{skip, true}]), % {skip, true} has to be set on windows

    wxPanel:setSizer(Panel, MainSizer),
    {Panel, #state{parent=Panel, config=Config, notebook = Notebook , textctrl1=TextCtrl1 , textctrl2=TextCtrl2 , textctrl3=TextCtrl3, textctrl4=TextCtrl4 }}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Async Events are handled in handle_event as in handle_info

handle_event(#wx{event = #wxNotebook{type = command_notebook_page_changed}},
    State = #state{notebook = Notebook}) ->
    Selection = wxNotebook:getSelection(Notebook),
    Title = wxNotebook:getPageText(Notebook, Selection),
    snmpe1:format(State#state.config,"You have selected the tab ~p\n",[Title]),


    {noreply,State};

%% For button event
handle_event(#wx{id=?wxID_SAVE, event=#wxCommand{type=command_button_clicked}}, 
    State = #state{parent=Parent}) ->
    B_save = wxWindow:findWindowById(?wxID_SAVE, [{parent, Parent}]),
    Butt = wx:typeCast(B_save, wxButton),
    Label = wxButton:getLabel(Butt),
    save_conf(State),
    snmpe1:format(State#state.config,"Button_save: \'~tp\' clicked~n",[Label]),
    {noreply,State};


handle_event(#wx{event=#wxCommand{type=command_togglebutton_clicked}}, 
     State = #state{}) ->
     snmpe1:format(State#state.config,
		"Button: You toggled the 'Toggle button' ~n",[]),
    {noreply,State};

handle_event(#wx{id=Id, event=#wxCommand{type=command_button_clicked}}, 
    State = #state{parent=Parent}) ->
    Button = wxWindow:findWindowById(Id, [{parent, Parent}]),
    Butt = wx:typeCast(Button, wxButton),
    Label = wxButton:getLabel(Butt),
    snmpe1:format(State#state.config,"Button: \'~ts\' clicked~n",[Label]),
    {noreply,State};


handle_event(Ev = #wx{}, State = #state{}) ->
    snmpe1:format(State#state.config,"Got Event ~p\n",[Ev]),
    {noreply,State}.

%% Callbacks handled as normal gen_server callbacks
handle_info(Msg, State) ->
    snmpe1:format(State#state.config, "Got Info ~p\n",[Msg]),
    {noreply,State}.

handle_call(shutdown, _From, State=#state{parent=Panel}) ->
    wxPanel:destroy(Panel),
    {stop, normal, ok, State};

handle_call(Msg, _From, State) ->
    snmpe1:format(State#state.config,"Got Call ~p\n",[Msg]),
    {reply,ok,State}.

handle_cast(Msg, State) ->
    io:format("Got cast ~p~n",[Msg]),
    {noreply,State}.


code_change(_, _, State) ->
    {stop, ignore, State}.

terminate(_Reason, _State) ->
    ok.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Local functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

conf_area(Parent) ->

    FixedFont = wxFont:new(10, ?wxFONTFAMILY_TELETYPE, ?wxNORMAL, ?wxNORMAL,[]),
    Ed = wxStyledTextCtrl:new(Parent),
    ?stc:styleClearAll(Ed),
    ?stc:styleSetFont(Ed, ?wxSTC_STYLE_DEFAULT, FixedFont),
    ?stc:setLexer(Ed, ?wxSTC_LEX_ERLANG),
    ?stc:setMarginType(Ed, 0, ?wxSTC_MARGIN_NUMBER),
    LW = ?stc:textWidth(Ed, ?wxSTC_STYLE_LINENUMBER, "9"),
    ?stc:setMarginWidth(Ed, 0, LW),
    ?stc:setMarginWidth(Ed, 1, 0),

    ?stc:setSelectionMode(Ed, ?wxSTC_SEL_LINES),
    %%?stc:hideSelection(Ed, true),

    Styles =  [{?wxSTC_ERLANG_DEFAULT,  {0,0,0}},
	       {?wxSTC_ERLANG_COMMENT,  {160,53,35}},
	       {?wxSTC_ERLANG_VARIABLE, {150,100,40}},
	       {?wxSTC_ERLANG_NUMBER,   {5,5,100}},
	       {?wxSTC_ERLANG_KEYWORD,  {130,40,172}},
	       {?wxSTC_ERLANG_STRING,   {170,45,132}},
	       {?wxSTC_ERLANG_OPERATOR, {30,0,0}},
	       {?wxSTC_ERLANG_ATOM,     {0,0,0}},
	       {?wxSTC_ERLANG_FUNCTION_NAME, {64,102,244}},
	       {?wxSTC_ERLANG_CHARACTER,{236,155,172}},
	       {?wxSTC_ERLANG_MACRO,    {40,144,170}},
	       {?wxSTC_ERLANG_RECORD,   {40,100,20}},
	       {?wxSTC_ERLANG_SEPARATOR,{0,0,0}},
	       {?wxSTC_ERLANG_NODE_NAME,{0,0,0}},
	       %% Optional 2.9 stuff
	       {?wxSTC_ERLANG_COMMENT_FUNCTION, {160,53,35}},
	       {?wxSTC_ERLANG_COMMENT_MODULE, {160,53,35}},
	       {?wxSTC_ERLANG_COMMENT_DOC, {160,53,35}},
	       {?wxSTC_ERLANG_COMMENT_DOC_MACRO, {160,53,35}},
	       {?wxSTC_ERLANG_ATOM_QUOTED, {0,0,0}},
	       {?wxSTC_ERLANG_MACRO_QUOTED, {40,144,170}},
	       {?wxSTC_ERLANG_RECORD_QUOTED, {40,100,20}},
	       {?wxSTC_ERLANG_NODE_NAME_QUOTED, {0,0,0}},
	       {?wxSTC_ERLANG_BIFS, {130,40,172}},
	       {?wxSTC_ERLANG_MODULES, {64,102,244}},
	       {?wxSTC_ERLANG_MODULES_ATT, {64,102,244}}
	      ],
    SetStyle = fun({Style, Color}) ->
		       ?stc:styleSetFont(Ed, Style, FixedFont),
		       ?stc:styleSetForeground(Ed, Style, Color)
	       end,
    [SetStyle(Style) || Style <- Styles],
    ?stc:setKeyWords(Ed, 0, keyWords()),
    
    %% Scrolling
    Policy = ?wxSTC_CARET_SLOP bor ?wxSTC_CARET_JUMPS bor ?wxSTC_CARET_EVEN, 
    ?stc:setYCaretPolicy(Ed, Policy, 3),
    ?stc:setVisiblePolicy(Ed, Policy, 3),

    %% ?stc:connect(Ed, stc_doubleclick),
    ?stc:setReadOnly(Ed, true),
%     io:format("click -----> ~p ~n", [Ed]),
    Ed.

load_conf(Ed, {ok, Code}) ->
    ?stc:setReadOnly(Ed, false),
    ?stc:setTextRaw(Ed, <<Code/binary, 0:8>>),
    Lines = ?stc:getLineCount(Ed),
    Sz = trunc(math:log10(Lines))+1,
    LW = ?stc:textWidth(Ed, ?wxSTC_STYLE_LINENUMBER, lists:duplicate(Sz, $9)),
    %%io:format("~p ~p ~p~n", [Lines, Sz, LW]),
    ?stc:setMarginWidth(Ed, 0, LW+5),
%    ?stc:setReadOnly(Ed, true),
    Ed.

save_conf(Stat=#state{})->
% backup curent config files
    Dir_backup="./conf/backup",
    
    Agents=?stc:getText(Stat#state.textctrl1),
    file:write_file("./conf/backup/agents.conf",Agents,[binary]),
    Users=?stc:getText(Stat#state.textctrl2),
    file:write_file("./conf/backup/users.conf",Users,[binary]),    
    Mgmt=?stc:getText(Stat#state.textctrl3),	
    file:write_file("./conf/backup/manager.conf",Mgmt,[binary]),
% backup snmpm_config_db
    snmpm_config:backup(Dir_backup),

    %% -- Manager SNMP config --
%    MgrConf = read_manager_config_file(Dir_backup),
%    init_manager_config(MgrConf),

    %% -- User config --
%    Users = read_users_config_file(Dir_backup),
%    init_users_config(Users),

    %% -- Agents config --
%    Agents = read_agents_config_file(Dir_backup),
%    init_agents_config(Agents),


    Agents=?stc:getText(Stat#state.textctrl1),
    file:write_file("./conf/agents.conf",Agents,[binary]),
    Users=?stc:getText(Stat#state.textctrl2),
    file:write_file("./conf/users.conf",Users,[binary]),    
    Mgmt=?stc:getText(Stat#state.textctrl3),	
    file:write_file("./conf/manager.conf",Mgmt,[binary]),
    Query=?stc:getText(Stat#state.textctrl4),
    file:write_file("./conf/querys_m.conf",Query,[binary]).


%unload_conf(Ed) ->
%    ?stc:setReadOnly(Ed, false),
%    ?stc:setTextRaw(Ed, <<0:8>>),
%    ?stc:setReadOnly(Ed, true),
%    Ed.

keyWords() ->
    L = ["after","begin","case","try","cond","catch","andalso","orelse",
	 "end","fun","if","let","of","query","receive","when","bnot","not",
	 "div","rem","band","and","bor","bxor","bsl","bsr","or","xor"],
    lists:flatten([K ++ " " || K <- L] ++ [0]).
