%% %CopyrightBegin%
%% 
%% Copyright Ericsson AB 2009-2012. All Rights Reserved.
%% 
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%% 
%% %CopyrightEnd%

%% @docfile "snmpe.hrl"
%% @author A. A. Gog <a.gog@list.ru>
%% @copyright 2020 A. A. Gog


-module(snmpe_get_info).
 
-behaviour(wx_object).

-export([start/1, init/1, terminate/2,  code_change/3, config_ets/3,
	 handle_info/2, handle_call/3, handle_cast/2, handle_event/2, snmp_get/4]).

-include_lib("wx/include/wx.hrl").
-include_lib("stdlib/include/ms_transform.hrl").
-include_lib("snmp/include/RFC1213-MIB.hrl").
-include_lib("./include/snmpe.hrl").

-define(Get,10).
-define(Get_Save,11).
-define(Get_bulk,12).
-define(stc, wxStyledTextCtrl).
-define(stt, wxTreeCtrl).
-define(stl, wxListCtrl).
-define(TABLE_M,snmpm_query_m).
-define(CONF_M,"./conf/querys_m.conf").
-define(Bulk_N,100).

-record(state, 
	{
	  parent,
	  config,
	  command
	 }).

start(Config) ->
    wx_object:start_link(?MODULE, Config, []).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
init(Config) ->
        wx:batch(fun() -> do_init(Config) end).

do_init(Config) ->
    Parent = proplists:get_value(parent, Config),  
    Panel = wxPanel:new(Parent, []),

    %% Setup sizers
    MainSizer = wxBoxSizer:new(?wxVERTICAL),
    Sizer = wxStaticBoxSizer:new(?wxHORIZONTAL, Panel, 
				 [{label, "Command single line"}]),
   Ip_addr_Sizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				 [{label, "Ip addr"}]),
   Snmp_ver_Sizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				 [{label, "snmp Ver"}]),
   Community_Sizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				 [{label, "Community"}]),
   Port_Sizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				 [{label, "Port"}]),
   Oid_Sizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				 [{label, "Oid"}]),
   Run_Sizer= wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				 [{label, "Run test"}]),
   Save_Sizer= wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				 [{label, "Save to file"}]),
    B_get = wxButton:new(Panel, 10, [{label,"Get bulk"}]),
    wxButton:setToolTip(B_get, "Get snmp data"),

    Save_get = wxButton:new(Panel, 11, [{label,"Add oid to agents"}]),
    wxButton:setToolTip(Save_get, "Save get single snmp data"),

    Sizer3 = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				  [{label, "Snmp results"}]),

%% Create a wxChoice

   Ip_Choice = wxTextCtrl:new(Panel, 14, [{value, ?Ip_addr},{size,{150,20}},{style, ?wxDEFAULT}]),
   wxChoice:setToolTip(Ip_Choice, "Ip"),
   Snmp_Ver_Choice = wxChoice:new(Panel, 15, [{size,{50,20}},{choices, ?snmp_ver}, {pos,{0,0}}]),
   wxChoice:setSelection(Snmp_Ver_Choice,1),
   wxChoice:setToolTip(Snmp_Ver_Choice, "SNMP v."),
   Community_Choice = wxChoice:new(Panel, 16, [{size,{150,20}},{choices, ?snmp_com}, {pos,{0,0}}]),   
   wxChoice:setSelection(Community_Choice,0),
%   Community_Choice = wxTextCtrl:new(Panel, 16, [{value, ?Community},{size,{150,20}},{style, ?wxDEFAULT}]),
   wxChoice:setToolTip(Community_Choice, "Community"),
%   Port_Choice = wxStyledTextCtrl:new(Panel, [{size,{70,20}},{id,17}]),
   Port_Choice = wxTextCtrl:new(Panel, 17, [{value, ?Port},{size,{70,20}},{style, ?wxDEFAULT}]),
   wxChoice:setToolTip(Port_Choice, "Port"),
   Oid_Choice = wxChoice:new(Panel, 18, [{size,{200,20}},{choices, ?sysList}, {pos,{0,0}}]),
   wxChoice:setSelection(Oid_Choice,0),
   wxChoice:setToolTip(Oid_Choice, "Oid default"),
 
% out results  ListCtrl3 = ?stt:new(Panel,[{id,3}]),
    WList = ?stl:new(Panel,[{winid,3},{style, ?wxLC_REPORT}]),

    wxChoice:connect(Oid_Choice,command_choice_selected),
    wxWindow:connect(Panel, command_button_clicked),
    wxListCtrl:connect(WList, command_list_item_selected,[]),    

    %% Add to sizers
    wxSizer:add(Sizer,Ip_addr_Sizer, []),
    wxSizer:add(Sizer,Snmp_ver_Sizer, []),
    wxSizer:add(Sizer,Community_Sizer, []),
    wxSizer:add(Sizer,Port_Sizer, []),
    wxSizer:add(Sizer,Oid_Sizer, []),
    wxSizer:add(Sizer,Run_Sizer, []),
    wxSizer:add(Sizer,Save_Sizer, []),
    wxSizer:add(Ip_addr_Sizer, Ip_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Snmp_ver_Sizer, Snmp_Ver_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Community_Sizer, Community_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Port_Sizer,Port_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Oid_Sizer, Oid_Choice,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Run_Sizer, B_get,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Save_Sizer, Save_get,  [{flag, ?wxEXPAND}]),
    wxSizer:add(Sizer3, WList, [{flag, ?wxEXPAND}, {proportion, 1}]),
    wxSizer:add(MainSizer, Sizer,  [{flag, ?wxEXPAND}]),
    wxSizer:add(MainSizer, Sizer3, [{flag, ?wxEXPAND}, {proportion, 1}]),
    wxPanel:setSizer(Panel, MainSizer),
    {Panel, #state{parent=Panel, config=Config}}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Async Events are handled in handle_event as in handle_info
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%GET button
handle_event((#wx{id=?Get , event=#wxCommand{type=command_button_clicked}}),
	     State=#state{parent=Parent}) -> get_save(get,State,Parent);

%%SAVE button
handle_event((#wx{id=?Get_Save , event=#wxCommand{type=command_button_clicked}}),
	     State = #state{parent=Parent}) -> get_save(save,State,Parent);

%%GET_BULK
handle_event(#wx{id=?Get_bulk, event=#wxCommand{type=command_button_clicked}}, 
    State = #state{parent=Parent}) ->
    B0 = wxWindow:findWindowById(?Get_bulk, [{parent, Parent}]),
    Butt = wx:typeCast(B0, wxButton),
    Label = wxButton:getLabel(Butt),
    snmpe1:format(State#state.config,"Button: \'~ts\' clicked~n",[Label]),
    {noreply,State};


handle_event(#wx{id=16, event=#wxCommand{type=command_button_clicked}}, 
    State = #state{parent=Parent}) ->
    B0 = wxWindow:findWindowById(16, [{parent, Parent}]),
    Butt = wx:typeCast(B0, wxButton),
    Label = wxButton:getLabel(Butt),
    snmpe1:format(State#state.config,"Button: \'~ts\' clicked~n",[Label]),
    {noreply,State};

%% select oid
handle_event(#wx{id=18, event = #wxCommand{type = command_choice_selected ,cmdString = Value}},
    State = #state{}) ->
    Oid_atom=list_to_atom(Value),
    {ok,Oid} = snmpm:name_to_oid(Oid_atom),
    snmpe1:format(State#state.config,"Selected Oid ~p\n",[Oid]),
    {noreply, State#state{command=Oid}};

handle_event(#wx{id=3, event = #wxList{itemIndex = Item}, obj = WList},
    State = #state{}) ->
%    B0 = wxWindow:findWindowById(18, [{parent, Parent}]),
%    Oid_String=wx:typeCast(B0,wxTextCtrl),
    ItemTextT2 = wxListCtrl:getItemText(WList, Item),
%    K=string:tokens(ItemTextT2,"|"),
%    OidSel=lists:nth(1,K),
%    wxTextCtrl:setValue(NewOid,OidSel),
    OidSel=snmp_e_fun:wxTCtrl(ItemTextT2),
    snmpe1:format(State#state.config, "You have selected ~p.\n", [OidSel]),
    {noreply, State#state{command=OidSel}};

handle_event(Ev = #wx{}, State = #state{}) ->
    snmpe1:format(State#state.config,"Got Event ~p\n",[Ev]),
    {noreply, State}.

%% Callbacks handled as normal gen_server callbacks
handle_info(Msg, State) ->
    snmpe1:format(State#state.config, "Got Info ~p\n",[Msg]),
    {noreply, State}.

handle_call(shutdown, _From, State=#state{parent=Panel}) ->
    wxPanel:destroy(Panel),
    {stop, normal, ok, State};

handle_call(Msg, _From, State) ->
    snmpe1:format(State#state.config,"Got Call ~p\n",[Msg]),
    {reply, {error,nyi}, State}.

handle_cast(Msg, State) ->
    io:format("Got cast ~p~n",[Msg]),
    {noreply,State}.

code_change(_, _, State) ->
    {stop, ignore, State}.

terminate(_Reason, _State) ->
    ok.

get_save(Do,State,Parent)->
%#wx{event = #wxList{itemIndex = Item}
    Wx_IP = wxWindow:findWindowById(14, [{parent, Parent}]),
    Obj_IP = wx:typeCast(Wx_IP, wxTextCtrl),
    IP_String = wxTextCtrl:getValue(Obj_IP),
     
    {Res,IP_Name,IP_Addr}= snmp_e_fun:ip_name(IP_String),
   
%    IP2={10,241,7,152},
%    IP=lists:map(fun(X) -> {Int,_} =string:to_integer(X), Int end, string:tokens(IP_String, ".")),

    Wx_Ver = wxWindow:findWindowById(15, [{parent, Parent}]),
    Obj_Ver = wx:typeCast(Wx_Ver, wxChoice),
    N_Ver = wxChoice:getSelection(Obj_Ver),  
    VerStr=wxChoice:getString(Obj_Ver, N_Ver),
    Ver=force:to_atom(VerStr),

    Wx_Community = wxWindow:findWindowById(16, [{parent, Parent}]),
    Obj_Wx_Community = wx:typeCast(Wx_Community, wxChoice),
    Comm_N = wxChoice:getSelection(Obj_Wx_Community),
    Community=wxChoice:getString(Obj_Wx_Community,Comm_N),
 
    Wx_Port = wxWindow:findWindowById(17, [{parent, Parent}]),
    Obj_Port = wx:typeCast(Wx_Port, wxTextCtrl),
    Port_String = wxTextCtrl:getValue(Obj_Port), 
    Port=force:to_integer(Port_String),

    Wx_Oid = wxWindow:findWindowById(18, [{parent, Parent}]),
    Obj_Oid = wx:typeCast(Wx_Oid, wxChoice),
    N_Oid = wxChoice:getSelection(Obj_Oid),  
    OidStr=wxChoice:getString(Obj_Oid, N_Oid),
    Oid_atom=force:to_atom(OidStr),

    {ok,Oid} = snmpm:name_to_oid(Oid_atom),

%%    Oid=State#state.command,

    CtrlOut=wxWindow:findWindowById(3, [{parent, Parent}]),
    WList = wx:typeCast(CtrlOut, ?stl),

%AgentTestConfig =
%    	[{tdomain,snmpUDPDomain},
%	 {reg_type,target_name},
%	 {taddress,{IP,Port}},
%	 {engine_id,"engine_test"},
%	 {timeout, 100000},           % Timeout
%	 {max_message_size, 1000},    % Max message (packet) size
%	 {version, v2 },              % MPModel
%	 {sec_model, Ver},            % SecModel
%	 {sec_name, "initial"},       % SecName
%	 {sec_level, noAuthNoPriv},   % SecLevel
%	 {community, Community}],     % Community

 {ok,ConfDir}=snmpm_config:system_info(config_dir),  

 Hash=erlang:binary_to_list(base64:encode(crypto:strong_rand_bytes(15))),  

 Conf={simple_user,Hash, Community, IP_Addr, Port, IP_Name, infinity, 10000000, v2, Ver, "initial", noAuthNoPriv},
 
 AppendConf={[ConfDir],Conf},
 
 {ok,{simple_user,Hash,AgentTestConfig,v2}}=snmpm_config:check_agent_config(Conf),

% io:format("AgentTestConfig:~n~p~n,Hash:~p~n",[AgentTestConfig,Hash]),
 snmpm_config:register_agent(simple_user,Hash,AgentTestConfig), 
 wx_misc:beginBusyCursor(),
% snmp_get(Hash,[Oid],TreeCtrl3,tree),
% ?stl:clearAll(WList),

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 snmp_get2(Hash,[Oid],WList,list,State),
 wx_misc:endBusyCursor(),

% io:format("Res ------------------------------------: ~p~n",[Res]),
  {Label2,RES2}= make_get(Res,Do,Hash,AppendConf,State),
    snmpe1:format(State#state.config,"Button: \'~ts\' clicked ~p~n",[Label2,RES2]),
    {noreply,State}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Local functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

 make_get(St,That,Hash,{ConfDir,Config},State)->
 Oid=State#state.command,
 {RES,Stat} =   case {St,That} of
	{ok,save} -> {add_agent_conf (ConfDir,Config,Oid,State),"Save agents"};
        {_,save} -> {add_agent_conf (ConfDir,Config,Oid,State),"Not resolve IP address"};
	{_,get} ->  snmpm_config:unregister_agent(simple_user,Hash), 
	{"OK","Get test"}
			
    end,
 {RES,Stat}.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%----------------------------------------------
 snmp_get(Agent,[Oid],TreeCtrl3,tree)->
     case (catch snmp_e_manager:sync_get_bulk(Agent,0,?Bulk_N,[Oid])) of
        {'EXIT',_W} ->?stc:setText(TreeCtrl3,io_lib:format("Error EXIT: ~p~n",[_W])),
		     error ;
	{ok,Tesing,_Timer}->
	     RootText=io_lib:format("~p",[Oid]),
	     RootF=lists:flatten(RootText),
             {ok,ConfigDir}=snmpm_config:system_info(config_dir),
             config_ets(ConfigDir,?CONF_M,?TABLE_M ),
	     ?stt:deleteAllItems(TreeCtrl3),
	     RootId = wxTreeCtrl:addRoot(TreeCtrl3,RootF),
	     io:format("Oid=~p~n",[Oid]),
	     snmp_e_fun:out_vbs_t(Tesing,test,TreeCtrl3,RootId,Oid),  
	     ?stt:selectItem(TreeCtrl3, RootId),
		    ok;
	{_,_W} ->wxTreeCtrl:addRoot(TreeCtrl3,io_lib:format("Error: ~p~n",[_W])),
 
	error 
    end.

%----------------------------------------------
 snmp_get2(Agent,[Oid],WList,list,State)->
%    io:format("~nOID: ~p~n",[Oid]),
%    snmp_e_manager:sync_get_bulk(Agent,0,?Bulk_N,[Oid]))
     case (catch snmp_e_manager:sync_get_bulk(Agent,0,200,Oid)) of
        {'EXIT',_W} ->snmpe1:format(State#state.config,"Error: \'~ts\' ~p~n",[Oid,_W]),
		     error ;
	{ok,Tesing,_Timer}->
             {ok,ConfigDir}=snmpm_config:system_info(config_dir),
             config_ets(ConfigDir,?CONF_M,?TABLE_M ),
             ?stl:clearAll(WList),

	     wxListCtrl:insertColumn(WList, 0, "Oid", []),
	     wxListCtrl:insertColumn(WList, 1, "Type", []),
	     wxListCtrl:insertColumn(WList, 2, "Value", []),
	     wxListCtrl:setColumnWidth(WList, 0, 200),
	     wxListCtrl:setColumnWidth(WList, 1, 120),
	     wxListCtrl:setColumnWidth(WList, 2, 800),

	     snmp_e_fun:out_vbs(Tesing,test,WList),  
       	    ok;
	 _M ->snmpe1:format(State#state.config,"EXIT: \'~ts\' ~p~n",["",_M]),
	      error
    end.


%---------------------------------------------
%% Add agent config file
%---------------------------------------------
 add_agent_conf (ConfigDir,Conf,Oid1,State) -> 
% Conf= {simple_user,Hash, Community, IP_Addr, Port, IP_Name, infinity, 1000, v2, Ver, "initial", noAuthNoPriv}
% Taddress=[{element(4,Conf),element(5,Conf)}],
% Fn = ets:fun2ms(fun({M,N}) when N == Taddress ->M end),
 Ress1=ets:select(snmpm_agent_table,[{{'$1','$2'},[{'==','$2',{const,{element(4,Conf),element(5,Conf)}}}],['$1']}]),
 case lists:flatlength(Ress1)>0  of
    true -> error ,
	     snmpe1:format(State#state.config,"Allready agent in use: ~p ~n",[Ress1]);
    false  -> snmpm_conf:append_agents_config(ConfigDir,[Conf]),
	     snmpe1:format(State#state.config,"Add agent for use: ~p ~n",[Ress1])
 end, 
 add_query_conf(ConfigDir,Conf,Oid1,State),
 ok.

%---------------------------------------------
%% Work add to config files
%--------------------------------------------
 add_query_conf(ConfigDir,Conf,Oid2,State)->
%% recreate table snmpm_query_m
  io:format("Conf---: ~p~n Oid2: ~p~n", [Conf,Oid2]),
  Table=?TABLE_M,
  File_conf=?CONF_M,

  config_ets(ConfigDir, File_conf,Table),
%  snmpm_config:unregister_agent(element(1,Conf),element(2,Conf)),
  CountA=ets:select(snmpm_agent_table,[{{'$1','$2'},[{'==','$2',{const,{element(4,Conf),element(5,Conf)}}}],['$1']}]),
   ID= case length(CountA) >=2  of
	   false -> 
	       io:format(">>>>>>>> ~p~n",[Conf]),
	       snmpm_conf:append_agents_config(ConfigDir,[Conf]),
	       element(2,Conf);   
	   true -> 
	       FindA=lists:last(CountA),
	       FinID =case element(2,Conf) == element(1,FindA) of 
		       true -> CountA;
	     	       false ->	lists:reverse(CountA)
	       end,
		   [{H,_}|_T] = FinID,	     
		   snmpm_config:unregister_agent(element(1,Conf),element(2,Conf)),
		   H
        end,  
%   io:format("Conf--ID: ~p ID=~p CountA=~p~n", [ID,element(1,Conf),CountA]),

%    ID=element(2,Conf),
    IP=element(4,Conf),
    Port=element(5,Conf),
    NameIP=element(6,Conf),

    Conf_txt=io_lib:format("{~p, 0, 1, ~p, ~p, ~p, ~p}.\n",[ID,Oid2,IP,Port,NameIP]),

 % ets snmpm_query_m {"/ic2huC3lxTx2O3DrZjX",0,1,[1,3,6],{10,241,7,149},161,"host fqdn"}
 case ets:select(?TABLE_M,[{{'$1','$2','$3','$4','$5','$6','$7'},[{'==','$5',{const,IP}},{'==','$4',{const,Oid2}}],['$$']}])==[] of
      true -> 
%         io:format("DIR=~p~n",[DirConf]),
	 file:write_file(?CONF_M,Conf_txt,[append]),
	 Conf_q={ID, 0,0,Oid2,IP,Port,NameIP},
%	 io:format("~nConf_q: ~p~n",[Conf_q]),
	 ets:insert_new(Table,Conf_q);
      false -> snmpe1:format(State#state.config,"IP Oid allready in query use : ~p , ~p ",[IP,Oid2])
 end,  
    ok.
%---------------------------------------------
%% Laad config to ets table if not load
%---------------------------------------------
config_ets (ConfigDir,File_conf,Table) ->
  case ets:info(Table,name)==Table of
    true ->  ok;
%    io:format("true: ~p~n", [Table]);
    false -> ets:new(Table, [bag,public,named_table]),
	     snmpe_ets:read_config_file(ConfigDir,File_conf,Table)
  end.


