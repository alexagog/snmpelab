%% %CopyrightBegin%
%% 
%% Copyright Ericsson AB 2009-2012. All Rights Reserved.
%% 
%% Licensed under the Apache License, Version 2.0 (the "License");
%% you may not use this file except in compliance with the License.
%% You may obtain a copy of the License at
%%
%%     http://www.apache.org/licenses/LICENSE-2.0
%%
%% Unless required by applicable law or agreed to in writing, software
%% distributed under the License is distributed on an "AS IS" BASIS,
%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%% See the License for the specific language governing permissions and
%% limitations under the License.
%% 
%% %CopyrightEnd%

%% @docfile "snmpe.hrl"
%% @author A. A. Gog <a.gog@list.ru>
%% @copyright 2020 A. A. Gog
 
-module(snmpe_ets).
-author('a.gog@list.ru').
%% -compile(exportall).
-behaviour(wx_object).
-include_lib("kernel/include/file.hrl").
-include_lib("include/snmp_debug.hrl").
-compile({no_auto_import,[error/1]}).

-export([start/1, init/1, terminate/2,  code_change/3, 
        handle_info/2, handle_call/3, handle_cast/2, handle_event/2,intro/0, error/1]).
-export([read_config_file/3]).

-export_type([order_config_entry_function/0]).
-type(order_config_entry_function() ::
	fun((term(), term()) -> boolean())).



-include_lib("wx/include/wx.hrl").
-include("include/snmp_types.hrl").
-include_lib("kernel/include/file.hrl").


%%-define(ABOUT, (?wxID_ABOUT)).
-define(EXIT, (?wxID_EXIT)).
-define(GET, 200).
-define(OPEN_FILE,(?wxID_OPEN)).
-define(CLOSE_FILE,(?wxID_CLOSE)).
-define(SAVE_FILE, (?wxID_SAVE)).
-define(TABLE_M,snmpm_query_m).
-define(CONF_M,"querys_m.conf").

-define(stc, wxStyledTextCtrl).

-define(GRID, 400).


%% For wx-2.9 usage
-ifndef(wxSTC_ERLANG_COMMENT_FUNCTION).
-define(wxSTC_ERLANG_COMMENT_FUNCTION, 14).
-define(wxSTC_ERLANG_COMMENT_MODULE, 15).
-define(wxSTC_ERLANG_COMMENT_DOC, 16).
-define(wxSTC_ERLANG_COMMENT_DOC_MACRO, 17).
-define(wxSTC_ERLANG_ATOM_QUOTED, 18).
-define(wxSTC_ERLANG_MACRO_QUOTED, 19).
-define(wxSTC_ERLANG_RECORD_QUOTED, 20).
-define(wxSTC_ERLANG_NODE_NAME_QUOTED, 21).
-define(wxSTC_ERLANG_BIFS, 22).
-define(wxSTC_ERLANG_MODULES, 23).
-define(wxSTC_ERLANG_MODULES_ATT, 24).
-endif.



%% Основная функция: запускает wx-server, создаёт графические объекты,
%% выводит на экран приложения и обрабатывает запрос на завершение
%% приложения и проводит очистку ресурсов.

%% return record

-record(opt, {type=ets,
	      sys_hidden=true,
	      unread_hidden=true,
	      sort_key=2,
	      sort_incr=true
	     }).

-record(state, 
	{
	  parent,
	  grid,
	  node=node(),
	  opt=#opt{},
	  selected,
	  tabs,
	  timer,
	  config,
	  notebook,
	  note_win1,
	  note_win2,
	  note_win3,
	  note_win4,
	  note_win5,
          textctrl1,
	  textctrl2,
	  textctrl3,
	  textctrl4
	 }).

start(Config) ->
    wx_object:start_link(?MODULE, Config, []).


init(Config) ->
    wx:batch(fun() -> do_init(Config) end).


%% Main init

do_init(Config) ->
    Parent = proplists:get_value(parent, Config),  
    Panel = wxScrolledWindow:new(Parent),


%% Setup sizers
    MainSizer = wxStaticBoxSizer:new(?wxVERTICAL, Panel, 
				     [{label, "Configure"}]),

    ExpandFlag  = [{proportion, 1}, {border, 1}, {flag, ?wxEXPAND}],
    wxWindow:connect(Panel, command_button_clicked),


    Notebook = wxNotebook:new(Panel, 1, [{style, ?wxNB_DEFAULT bor ?wxNB_MULTILINE}]),

					        %?wxBK_ALIGN_MASK,
					        %?wxBK_TOP,
					        %?wxBK_BOTTOM,
					        %?wxBK_LEFT,
					        %?wxBK_RIGHT,
					        %?wxNB_MULTILINE % windows only


    [{_,Dir}]=ets:lookup(snmpm_config_table,config_db_dir),

    Node=erlang:node(),
    Type=ets,

%% =======================================================================================================

%% Agent.conf

    Grid_1 = wxPanel:new(Notebook,[]),
    Table_1=snmpm_agent_table,
    Win_conf1=observer_tv_t:start_link(Grid_1,[{node,Node}, {type,Type}, {table,Table_1},{config,Config}]), 
    wxNotebook:addPage(Notebook, Win_conf1, "agent", []),

%% user.conf

    Grid_2 = wxPanel:new(Notebook, []),
    Table_2=snmpm_user_table,
    Win_conf2=observer_tv_t:start_link(Grid_2,[{node,Node}, {type,Type}, {table,Table_2},{config,Config}]), 
    wxNotebook:addPage(Notebook, Win_conf2, "user", []),

%% manger.conf

    Grid_3 = wxPanel:new(Notebook, []),
    Table_3=snmpm_config_table,
    Win_conf3=observer_tv_t:start_link(Grid_3,[{node,Node}, {type,Type}, {table,Table_3},{config,Config}]), 
    wxNotebook:addPage(Notebook, Win_conf3, "config", []),


  Table=?TABLE_M,
  case ets:info(Table) of
    [_] ->  ets:delete_all_objects(Table);
    undefined -> ets:new(Table, [bag,public,named_table])
  end,

%% snmpm_cofig_file

    read_config_file(Dir,?CONF_M,Table),

    Grid_4 = wxPanel:new(Notebook, []),
    Table_4=snmpm_query_m,
    Win_conf4=observer_tv_t:start_link(Grid_4,[{node,Node}, {type,Type}, {table,Table_4},{config,Config}]), 
    wxNotebook:addPage(Notebook, Win_conf4, "query", []),

%% snmpm_mib_table

    Grid_5 = wxPanel:new(Notebook, []),
    Table_5=snmpm_mib_table,
    Win_conf5=observer_tv_t:start_link(Grid_5,[{node,Node}, {type,Type}, {table,Table_5},{config,Config}]), 
    wxNotebook:addPage(Notebook, Win_conf5, "mib", []),

%% ===============================================================================

    %% Add to sizers
    [wxSizer:add(MainSizer, Items, Flag) || {Items, Flag} <- [{Notebook,ExpandFlag}]],
    wxWindow:connect(Panel, command_button_clicked),
    wxNotebook:connect(Notebook, command_notebook_page_changed,
		       [{skip, true}]), % {skip, true} has to be set on windows

    wxPanel:setSizer(Panel, MainSizer),

    {Panel, #state{parent=Panel,config=Config,notebook = Notebook,
		   note_win1=Grid_1,
		   note_win2=Grid_2,
		   note_win3=Grid_3,
		   note_win4=Grid_4,
		   note_win5=Grid_5
		   }}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Async Events are handled in handle_event as in handle_info
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% @spec handle_event(noreply,State)-> {noreply,State}
		   
handle_event(#wx{event = #wxNotebook{type = command_notebook_page_changed}},
    State = #state{notebook = Notebook}) ->
    Selection = wxNotebook:getSelection(Notebook),
    Title = wxNotebook:getPageText(Notebook, Selection),
    snmpe1:format(State#state.config,"You have selected the tab ~p\n",[Title]),
    {noreply,State};

%% For button event
handle_event(#wx{id=?wxID_SAVE, event=#wxCommand{type=command_button_clicked}}, 
    State = #state{parent=Parent}) ->
    B_save = wxWindow:findWindowById(?wxID_SAVE, [{parent, Parent}]),
    Butt = wx:typeCast(B_save, wxButton),
    wxButton:getLabel(Butt),
    save_conf(State),
    {noreply,State};


handle_event(#wx{event=#wxCommand{type=command_togglebutton_clicked}}, 
    State = #state{}) ->
    snmpe1:format(State#state.config,"Button: You toggled the 'Toggle button' ~n",[]),
    {noreply,State};

%% @doc out snmpe1:format Button press
handle_event(#wx{id=Id, event=#wxCommand{type=command_button_clicked}}, 
    State = #state{parent=Parent}) ->
    Button = wxWindow:findWindowById(Id, [{parent, Parent}]),
    Butt = wx:typeCast(Button, wxButton),
    Label = wxButton:getLabel(Butt),
    snmpe1:format(State#state.config,"Button: \'~ts\' clicked~n",[Label]),
    {noreply,State};

%% @doc all event -> only  snmpe1:format (event)
handle_event(Ev = #wx{}, State = #state{}) ->
    snmpe1:format(State#state.config,"Got Event ~p\n",[Ev]),
    {noreply,State}.

%% Callbacks handled as normal gen_server callbacks

%% @doc Msg -> snmpe1:format
handle_info(Msg, State) ->

    snmpe1:format(State#state.config, "Got Info ~p\n",[Msg]),
    {noreply,State}.

%% @doc shutdown wxPanel:destroy(Panel)
handle_call(shutdown, _From, State=#state{parent=Panel}) ->
    wxPanel:destroy(Panel),
    {stop, normal, ok, State};

%% @doc to snmpe1:format -> io:format("Got cast ~p~n",[Ms]
handle_call(Msg, _From, State) ->
    snmpe1:format(State#state.config,"Got Call ~p\n",[Msg]),
    {reply,ok,State}.

%% @doc (Msg, State) -> io:format("Got cast ~p~n",[Msg])
handle_cast(Msg, State) ->
    io:format("Got cast ~p~n",[Msg]),
    {noreply,State}.

%% @doc (_, _, State) ->{stop, ignore, State}
code_change(_, _, State) ->
    {stop, ignore, State}.

terminate(_Reason, _State) ->
    ok.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Local functions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% verifiy and save conf ================================

save_conf(Stat=#state{})->

    Note1=Stat#state.textctrl1,
    Note2=Stat#state.textctrl2,
    Note3=Stat#state.textctrl3,
    Note4=Stat#state.textctrl4,
%%        io:formant("Curent line ~p~n",[[?stc:getCurLine(Note1)]]),
         LN=?stc:getLine(Note2,1),
         io:format("Get 2 line user  ~p~n",[LN]),
%%        io:formant("Get Style bit ~p~n",[?stc:getStyleBits(Note1)]),


    [{_,Dir}]=ets:lookup(snmpm_config_table,config_db_dir),

    ?stc:saveFile(Note1,"./conf/agents.conf"), 
    case  snmpm_conf:read_agents_config(Dir) of
	{ok,_} -> ?stc:styleSetBackground(Note1,?wxSTC_STYLE_DEFAULT,{255,255,255,250});
%%        ?stc:saveFile(Note1,"agents.conf");
        {_,_Label} -> 
	    ?stc:undo(Note1),
%%        ?stc:styleSetBackground(Note1,0,{10,90,20}),
%%        ?stc:styleSetBackground(Note1,1,{130,20,90}),
%%        ?stc:styleSetBackground(Note1,2,{130,20,20}),
%%        ?stc:styleSetBackground(Note1,9,{40,20,20}),
%%        ?stc:styleSetBackground(Note1,10,{16,20,20}),
%%        ?stc:styleSetBackground(Note1,11,{130,200,20}),
%%        ?stc:styleSetBackground(Note1,12,{130,100,20}),
%%        ?stc:styleSetBackground(Note1,13,{130,90,20}),
%%        ?stc:styleSetBackground(Note1,14,{130,80,20}),
%%        ?stc:styleSetBackground(Note1,15,{130,70,20}),
%%        ?stc:styleSetBackground(Note1,16,{30,0,20}),
%%        ?stc:styleSetBackground(Note1,17,{130,20,20}),
%%        ?stc:styleSetBackground(Note1,18,{10,0,20}),
%%        ?stc:styleSetBackground(Note1,19,{0,20,60}),
%%        ?stc:styleSetBackground(Note1,20,{0,220,20}),
%%        ?stc:styleSetBackground(Note1,21,{230,0,20}),
%%        ?stc:styleSetBackground(Note1,22,{10,20,20}),
%%        ?stc:styleSetBackground(Note1,23,{30,0,20}),
%%        ?stc:styleSetBackground(Note1,24,{10,20,20}),
%%        ?stc:styleSetBackground(Note1,31,{130,20,0}),
%%        ?stc:styleSetBackground(Note1,8,{13,120,120}),
%%        ?stc:styleSetBackground(Note1,5,{130,220,20}),
%%        ?stc:styleSetBackground(Note1,22,{130,20,0}),
%%        ?stc:styleSetBackground(Note1,32,{10,20,20}),

        snmpe1:format(Stat#state.config,"Save ERROR: \'~tp\' clicked~n",[_Label]),
        ?stc:saveFile(Note1,"./conf/agents.conf")
    end, 

%% --------- ?stc:saveFile(Note2,"users.conf"), 
        ?stc:saveFile(Note2,"./conf/users.conf"),
    case  snmpm_conf:read_users_config(Dir) of
	{ok,_} -> ok;
       {_,_Label2} -> 
        ?stc:undo(Stat#state.textctrl2),
        snmpe1:format(Stat#state.config,"Save ERROR: \'~tp\' clicked~n",[_Label2]),
        ?stc:saveFile(Note2,"./conf/users.conf")
    end, 

%% --------- ?stc:saveFile(StatNote3,"manager.conf"), 
       ?stc:saveFile(Note3,"./conf/manager.conf"),
    case  snmpm_conf:read_manager_config(Dir) of
	{ok,_} -> ok;
        {_,_Label3} -> 
	?stc:undo(Note3),
        snmpe1:format(Stat#state.config,"Save ERROR: \'~tp\' clicked~n",[_Label3]),
        io:format("mmmm: ~p~n ",["ERORR and Restore"]), 
        ?stc:saveFile(Note3,"./conf/manager.conf")
    end, 

    Query=?stc:getText(Note4),
    file:write_file("./conf/querys_m.conf",Query,[binary]).




%% -------------------------------------------------
read_config_file(Dir,FileName,Tab)
  when is_list(Dir), is_list(FileName) ->
    case file:open(filename:join(Dir, FileName), [read]) of
	{ok, Fd} ->
	    try
		    lists:reverse(read_lines(Fd, [], 1,Tab))
	    catch
		Error ->
%%		    S = erlang:get_stacktrace(),
		    d("File read of ~s throwed: ~p~n    ~p~n",
		      [FileName, Error]),
		    {error, Error};
		T:E ->
%%		    S = erlang:get_stacktrace(),
		    d("File read of ~s exception: ~p:~p~n    ~p~n",
		      [FileName,T,E]),
		    {error, {failed_read, Dir, FileName, {T, E}}}
	    after
		file:close(Fd)
                                                       	    end;
	{error, Reason} ->
	    {error, {Reason, FileName}}
    end.

%% -------------------------------------------------
read_lines(Fd, Acc, StartLine,Tab) ->
    case read_and_parse_term(Fd, StartLine,Tab) of
	{ok, Term, EndLine} ->
	   read_lines(Fd, [{StartLine, Term, EndLine}|Acc], EndLine,Tab);
	{error, Error, EndLine} ->
            throw({failed_reading, StartLine, EndLine, Error});
        {eof, _EndLine} ->
	    Acc
    end.
read_and_parse_term(Fd, StartLine,Tab) ->
    case io:request(Fd, {get_until, "", erl_scan, tokens, [StartLine]}) of
	{ok, Tokens, EndLine} ->
	    case erl_parse:parse_term(Tokens) of
                {ok, Term} ->
                    ets:insert(Tab,Term),
                    {ok, Term, EndLine};
                {error, {Line, erl_parse, Error}} ->
                    {error, {parse_error, Error}, Line}
            end;
        Other ->
            Other
    end.
%% -info

intro() ->
    i("~nSimple SNMP  tool v2 (version)"),
    i("------------------------------------------------"),
    i("Note: Non-trivial configurations still has to be"),
    i("      done manually. "),
    i("------------------------------------------------"),
    ok.

%% upgrade------------------------------------------------

d(F, A) ->
    i("DBG: " ++ F, A).
i(F) ->
    i(F, []).
i(F, A) -> 
    io:format(F ++ "~n", A).
error(R) ->
    throw({error, R}).
